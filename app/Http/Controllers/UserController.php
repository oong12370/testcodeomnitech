<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class UserController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = User::get();
        return view('admin.user.index')->with('getData', $getData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
 
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'gender' => 'required',
            ]);

            
            $tambah = new User;
            $tambah->name = $request->name;
            $tambah->lastName = $request->lastName;
            $tambah->email = $request->email;
            $tambah->DateOfbirth = $request->DateOfbirth;
            $tambah->password = Hash::make($request->password);
            $tambah->gender = $request->gender;
            $tambah->save();

            return redirect('user')->with('success','Item created successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
            ]);
        $variabel = User::find($id);
        $variabel->name = $request->name;
        $variabel->lastName = $request->lastName;
        $variabel->email = $request->email;
        $variabel->DateOfbirth = $request->DateOfbirth;
        $variabel->gender = $request->gender;
        $variabel->save();
        return redirect('user')->with('success','Item created successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = User::find($id);
        $hapus->delete();
        return redirect('user')->with('success','Item created successfully!');
    }
}
