<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ProductModel;
use App\Model\CategoryModel;
use DB;
use Validator;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DB::select("select a.id, a.nmProduct, a.description, a.price ,a.imageProduct, b.nmCategory, a.created_at,a.updated_at from tbl_product a
INNER JOIN tbl_category b ON b.id=a.idCategori ");
        $getCategory = CategoryModel::get();
        return view('admin.product.index')->with('getData', $getData)->with('getCategory', $getCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $validation = Validator::make($request->all(), [
                'imageProduct' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);
            $this->validate($request, [
                'nmProduct' => 'required',
                'imageProduct' => 'required',
                'idCategori' => 'required',
                'price' => 'required',
            ]);
 
            $image = $request->file('imageProduct');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
            $variabel = new ProductModel;
            $variabel->nmProduct = request('nmProduct');
            $variabel->description = request('description');
            $variabel->imageProduct = $new_name;
            $variabel->idCategori = request('idCategori');
            $variabel->price = request('price');
            $variabel->save();

            return redirect('product')->with('success','Item created successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            $this->validate($request, [
                'nmProduct' => 'required',
                'idCategori' => 'required',
                'price' => 'required',
            ]);

            $variabel = ProductModel::find($id);
            $variabel->nmProduct = request('nmProduct');
            $variabel->description = request('description');
            $variabel->imageProduct = request('imageProduct');
            $variabel->idCategori = request('idCategori');
            $variabel->price = request('price');
            return redirect('product')->with('success','Item created successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $hapus = ProductModel::find($id);
        $hapus->delete();
        return redirect('product')->with('success','Item created successfully!');
    }
}
