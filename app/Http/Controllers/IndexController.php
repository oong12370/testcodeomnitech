<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{
    public function login()
    {
        return view('index');
    }

    public function logouts()
    {
        auth()->logout();
        return redirect()->route('login.form');
    }
}
