<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Model\TransactioModel;
use App\Model\DetailtransactioModel;
use App\Model\StockModel;
class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tbl_stock')
            ->join('tbl_product', 'tbl_product.id', '=', 'tbl_stock.IdProduct')
            ->join('tbl_category', 'tbl_category.id', '=', 'tbl_product.idCategori')
            ->select('tbl_stock.*', 'tbl_stock.idTransaksi', 'tbl_product.nmProduct', 'tbl_product.imageProduct', 'tbl_category.nmCategory', 'tbl_stock.status' , 'tbl_stock.stock')
            ->orderBy('tbl_stock.idTransaksi', 'ASC')
            ->get();
        return view('admin.transaksi.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.transaksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'NoTransaksi' => 'required',
                'qty' => 'required',
                'idCategori' => 'IdProduct',
            ]);
 

            $variabel = new TransactioModel;
            $variabel->NoTransaksi = request('NoTransaksi');
            $variabel->StatusTransaksi = request('StatusTransaksi');
            $variabel->dateTarsaksi = request('dateTarsaksi');
            $variabel->idUser = request('idUser');
            $variabel->save();

            $IdProductS=request('IdProduct');
            $qty=request('qty');
            $idPembelian=request('idPembelian');
            $idTransaksi=request('NoTransaksi');
            $StatusTransaksi=request('StatusTransaksi');
            foreach($IdProductS as $key => $i){
                $var = new DetailtransactioModel;
                $var->idTransaksi =$idTransaksi;
                $var->IdProduct =$IdProductS[$key];
                $var->qty =$qty[$key];
                $var->idPembelian =$idPembelian[$key];
                $var->save();
            }

            
            foreach($IdProductS as $key => $i){
                $var = new StockModel;
                $var->idTransaksi =$idTransaksi;
                $var->status =$StatusTransaksi[$key];
                $var->IdProduct =$IdProductS[$key];
                $var->stock =$qty[$key];
                $var->save();
            }
            
            return redirect('transaksi')->with('success','Item created successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function searchProduct(Request $request)
    {
        $cari=$request->input('search');
        $search = DB::select("SELECT id, nmProduct,description,price FROM tbl_product WHERE nmProduct LIKE '%$cari%'"  );
        foreach ($search as $row) {
            $response[]= array("value"=>$row->id,"label"=>$row->nmProduct, "price"=>$row->price, "ids"=>$row->id);
          }
        return response()->json($response);
    }

}
