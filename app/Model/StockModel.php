<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StockModel extends Model
{
    
     
    protected $table = 'tbl_stock';

    protected $fillable = [
    	'idProduct',
        'idTransaksi',
        'status',
        'noTransaksi',
        'stock',
        'created_at',
    	'updated_at'
    ];
}
