<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactioModel extends Model
{
    
     
    protected $table = 'tbl_transaction';

    protected $fillable = [
    	'NoTransaksi',
        'status',
        'totalTransaksi',
        'idUser',
        'dateTarsaksi',
        'StatusTransaksi',
        'created_at',
    	'updated_at'
    ];
}
