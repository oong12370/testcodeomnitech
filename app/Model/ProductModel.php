<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
     
    protected $table = 'tbl_product';

    protected $fillable = [
    	'nmProduct',
        'description',
        'imageProduct',
        'idCategori',
        'price',
        'created_at',
    	'updated_at'
    ];
}
