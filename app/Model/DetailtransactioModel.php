<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailtransactioModel extends Model
{
    
     
    protected $table = 'tbl_detail_transaction';

    protected $fillable = [
    	'IdProduct',
        'qty',
        'idPembelian',
        'created_at',
    	'updated_at'
    ];
}
