<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    
    protected $table = 'tbl_category';

    protected $fillable = [
    	'nmCategory',
    	'description',
        'created_at',
    	'updated_at'
    ];
}
