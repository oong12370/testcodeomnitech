<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use DB;
use Illuminate\Support\Facades\Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
            if (Auth::check() != NULL) {
                $users=DB::select("SELECT a.id,a.image, b.name, b.email, a.motto  FROM image a 
                    INNER JOIN users b ON b.id = a.idUser WHERE b.id = ".Auth::user()->id."
                    ORDER BY a.id DESC");
                    //...with this variable
                    $view->with('users', $users );   
            } else {
                # code...
            }
             
        });  

    }
}
