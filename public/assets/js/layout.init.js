/*! jRespond.js v 0.10 | Author: Jeremy Fields [jeremy.fields@viget.com], 2013 | License: MIT */ ! function (a, b, c) {
    "object" == typeof module && module && "object" == typeof module.exports ? module.exports = c : (a[b] = c, "function" == typeof define && define.amd && define(b, [], function () {
        return c
    }))
}(this, "jRespond", function (a, b, c) {
    "use strict";
    return function (a) {
        var b = [],
            d = [],
            e = a,
            f = "",
            g = "",
            i = 0,
            j = 100,
            k = 500,
            l = k,
            m = function () {
                var a = 0;
                return a = "number" != typeof window.innerWidth ? 0 !== document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth : window.innerWidth
            },
            n = function (a) {
                if (a.length === c) o(a);
                else
                    for (var b = 0; b < a.length; b++) o(a[b])
            },
            o = function (a) {
                var e = a.breakpoint,
                    h = a.enter || c;
                b.push(a), d.push(!1), r(e) && (h !== c && h.call(null, {
                    entering: f,
                    exiting: g
                }), d[b.length - 1] = !0)
            },
            p = function () {
                for (var a = [], e = [], h = 0; h < b.length; h++) {
                    var i = b[h].breakpoint,
                        j = b[h].enter || c,
                        k = b[h].exit || c;
                    "*" === i ? (j !== c && a.push(j), k !== c && e.push(k)) : r(i) ? (j === c || d[h] || a.push(j), d[h] = !0) : (k !== c && d[h] && e.push(k), d[h] = !1)
                }
                for (var l = {
                        entering: f,
                        exiting: g
                    }, m = 0; m < e.length; m++) e[m].call(null, l);
                for (var n = 0; n < a.length; n++) a[n].call(null, l)
            },
            q = function (a) {
                for (var b = !1, c = 0; c < e.length; c++)
                    if (a >= e[c].enter && a <= e[c].exit) {
                        b = !0;
                        break
                    } b && f !== e[c].label ? (g = f, f = e[c].label, p()) : b || "" === f || (f = "", p())
            },
            r = function (a) {
                if ("object" == typeof a) {
                    if (a.join().indexOf(f) >= 0) return !0
                } else {
                    if ("*" === a) return !0;
                    if ("string" == typeof a && f === a) return !0
                }
            },
            s = function () {
                var a = m();
                a !== i ? (l = j, q(a)) : l = k, i = a, setTimeout(s, l)
            };
        return s(), {
            addFunc: function (a) {
                n(a)
            },
            getBreakpoint: function () {
                return f
            }
        }
    }
}(this, this.document));
jQuery(document).ready(function ($) {
    var pCon = $('.page-container'),
        sRight = $('.right-aside'),
        aRight = $('.right-aside'),
        mLbar = $('.m-leftbar-show'),
        pBody = $('body'),
        pHtml = $('html'),
        NoteCon = $('.note-container'),
        inboxCon = $('.inbox-container'),
        TaskCon = $('.task-container');


    $('.inbox-aside-action').on('click', function (event) {
        event.preventDefault();

        if (inboxCon.hasClass('hide-inbox-sidebar')) {
            inboxCon.removeClass('hide-inbox-sidebar');
            $(this).children('i').removeClass('fa-indent');
            $(this).children('i').addClass('fa-outdent');
        } else {
            inboxCon.addClass('hide-inbox-sidebar');
            $(this).children('i').removeClass('fa-outdent');
            $(this).children('i').addClass('fa-indent');
        }
        if (inboxCon.hasClass('mobile-mail-show')) {
            inboxCon.removeClass('mobile-mail-show');
        }

    });

    $('.item-label').each(function () {
        var labelColor = $(this).data('color');
        if (labelColor) {
            $(this).children('a').children('.label-color').css({
                'background-color': labelColor
            });
        }

    });

    var jRes = jRespond([{
        label: 'handheld',
        enter: 0,
        exit: 767
    }, {
        label: 'tablet',
        enter: 768,
        exit: 979
    }, {
        label: 'laptop',
        enter: 980,
        exit: 1199
    }, {
        label: 'desktop',
        enter: 1200,
        exit: 10000
    }]);

    jRes.addFunc([{
        breakpoint: ['handheld', 'tablet'],
        enter: function () {

            $('.email-list-details').on('click', function () {

                if (inboxCon.hasClass('mobile-mail-show')) {
                    inboxCon.removeClass('mobile-mail-show');
                } else {
                    inboxCon.addClass('mobile-mail-show');
                }

            });
            $('.task-list-details').on('click', function () {

                if (TaskCon.hasClass('mobile-task-show')) {
                    TaskCon.removeClass('mobile-task-show');

                } else {
                    TaskCon.addClass('mobile-task-show');
                    $('.task-list-back').fadeIn('fast');
                }

            });

            $('.task-list-back').on('click', function (event) {
                event.preventDefault();

                if (TaskCon.hasClass('mobile-task-show')) {
                    TaskCon.removeClass('mobile-task-show');
                    $('.task-list-back').fadeOut('fast');
                } else {
                    TaskCon.addClass('mobile-task-show');
                }

            });

            $('.note-list-details').on('click', function () {

                if (NoteCon.hasClass('mobile-note-show')) {
                    NoteCon.removeClass('mobile-note-show');

                } else {
                    NoteCon.addClass('mobile-note-show');
                    $('.note-list-back').fadeIn('fast');
                }

            });

            $('.note-list-back').on('click', function (event) {
                event.preventDefault();

                if (NoteCon.hasClass('mobile-note-show')) {
                    NoteCon.removeClass('mobile-note-show');
                    $('.note-list-back').fadeOut('fast');
                } else {
                    NoteCon.addClass('mobile-note-show');
                }

            });
            $('.mobile-mail-action').on('click', function (event) {
                event.preventDefault();

                if (inboxCon.hasClass('mobile-mail-show')) {
                    inboxCon.removeClass('mobile-mail-show');
                } else {
                    inboxCon.addClass('mobile-mail-show');
                }

            });



        },
        exit: function () {

        }
    }]);

    function composeForm() {
        var wh = $(window).height();
        var hHeight = $('.compose-form-top').height();
        var fHeight = $('.compose-form-bottom').height();
        var bHeight = wh - hHeight;
        var mHeight = (bHeight - fHeight) - 40;
        $('.compose-form-wrap').css({
            'height': mHeight + 'px'
        });

    }

    composeForm();

    function MaTmixInbox() {
        var wh = $(window).height();
        var inboxContainer = $('.inbox-container');
        if (inboxContainer.length) {
            var inboxTopOfset = inboxContainer.offset().top;

        }
        var inboxCalH = wh - inboxTopOfset
        var mailLenght = $('.email-content').length;


        $('.inbox-container').css({
            'height': inboxCalH + 'px'
        });
        $('.inbox-content').css({
            'height': inboxCalH + 'px'
        });
        $('.inbox-sidebar').css({
            'height': inboxCalH + 'px'
        });
        $('.email-list').css({
            'height': inboxCalH + 'px'
        });
        $('.mail-body').css({
            'height': inboxCalH + 'px'
        });

        $('.email-content').each(function () {
            if (mailLenght > 1) {

                $(this).parent('.mail-body').addClass('multi-page');
            }

        });

    }
    MaTmixInbox();

    $(window).smartresize(function () {
        composeForm();
        MaTmixInbox()
    });


    function MatMixScroll() {
        var wh = $(window).height();
        var AppsContainer = $('.apps-container');
        if (AppsContainer.length) {
            var AppsTopOfset = AppsContainer.offset().top;

        }
        var AppsCalH = wh - AppsTopOfset;


        $('.apps-panel-scroll').css({
            'height': AppsCalH + 'px'
        });

    }
    MatMixScroll();
    $(window).smartresize(function () {
        MatMixScroll()
    });


    var cFrm = $('.compose-form')
    $(document).on('click', '.compose-form-minimize', function (event) {
        event.preventDefault();
        var minCheck = $(this).parents('.compose-form-top').parents(cFrm);
        if (minCheck.hasClass('minimized')) {
            $("<span class='i-mask'></span>").prependTo(".page-container");
            $(".i-mask").css('cursor', 'url(images/close-icon.png),auto');
            minCheck.removeClass('minimized');
            pHtml.addClass('off-canvas');
            pBody.addClass('off-canvas');

        } else {
            minCheck.addClass('minimized');
            $('.i-mask').remove();
            pHtml.removeClass('off-canvas');
            pBody.removeClass('off-canvas');
        }


    });

    $(document).on('click', '.compose-form-maximize', function (event) {
        event.preventDefault();
        var minCheck = $(this).parents('.compose-form-top').parents(cFrm);
        if (minCheck.hasClass('maximized')) {
            minCheck.removeClass('maximized');
        } else {
            minCheck.addClass('maximized');
        }


    });

    $(document).on('click', '.compose-mail', function (event) {
        event.preventDefault();
        cFrm.addClass('compose-form-show');
        $("<span class='i-mask'></span>").prependTo(".page-container");
        $(".i-mask").css('cursor', 'url(images/close-icon.png),auto');
        pHtml.addClass('off-canvas');
        pBody.addClass('off-canvas');

        $('.compose-editor').summernote({
            height: 250,
            focus: true,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']]
            ]
        });

    });

    if ($.summernote) {
        $('.task-comment-input').summernote({
            height: 150,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']]
            ]
        });

        $('.forum-post-editor').summernote({
            height: 125,
            focus: true,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']]
            ]
        });

        $('.forum-reply-input').summernote({
            height: 225,
            focus: true,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['codeview']]
            ]
        });

    }


    $(document).on('click', '.compose-form-exit', function (event) {
        event.preventDefault();
        cFrm.removeClass('compose-form-show');
        cFrm.removeClass('minimized');
        pHtml.removeClass('off-canvas');
        pBody.removeClass('off-canvas');
        $('.i-mask').remove();
    });


    $(document).on('click', '.i-mask', function () {

        cFrm.removeClass('compose-form-show');
        pHtml.removeClass('off-canvas');
        pBody.removeClass('off-canvas');
        $('.i-mask').remove();
    });


    /**
     * iCheck
     * icheck.js
     * icheck.css
     *  */
    try {

        $('.mail-select,.select-all-email,.task-select,.select-all-task,.note-select,.select-all-note,.todo-select').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '30%' // optional
        });

        $('.i-min-check').iCheck({
            checkboxClass: 'icheckbox_minimal-pink',
            radioClass: 'iradio_minimal-pink',
            increaseArea: '30%' // optional
        });

        $('.tc-check,.tc-check-all').iCheck({
            checkboxClass: 'icheckbox_minimal-aero',
            radioClass: 'iradio_minimal-aero',
            increaseArea: '30%' // optional
        });

    } catch (e) {

    }

    function SelectAllMail() {
        var checkAll = $('input.select-all-email');
        var checkboxes = $('input.mail-select');



        checkAll.on('ifChecked ifUnchecked', function (event) {
            if (event.type == 'ifChecked') {
                checkboxes.iCheck('check');
            } else {
                checkboxes.iCheck('uncheck');
            }
        });

        checkboxes.on('ifChecked ifUnchecked', function (event) {
            if (event.type == 'ifChecked') {
                $(this).iCheck('check');
                $(this).parents('.email-list-action').parents('.email-list-item').addClass('m-selected');
            } else {
                $(this).iCheck('uncheck');
                $(this).parents('.email-list-action').parents('.email-list-item').removeClass('m-selected');
            }
        });

        checkboxes.on('ifChanged', function (event) {
            if (checkboxes.filter(':checked').length == checkboxes.length) {
                checkAll.prop('checked', 'checked');
            } else {
                checkAll.removeProp('checked');
            }
            checkAll.iCheck('update');
        });
    };

    SelectAllMail();


    function SelectAllNote() {
        var NoteCheckAll = $('input.select-all-note');
        var NoteCheckboxes = $('input.note-select');

        NoteCheckAll.on('ifChecked ifUnchecked', function (event) {
            if (event.type == 'ifChecked') {
                NoteCheckboxes.iCheck('check');
            } else {
                NoteCheckboxes.iCheck('uncheck');
            }
        });

        NoteCheckboxes.on('ifChecked ifUnchecked', function (event) {
            if (event.type == 'ifChecked') {
                $(this).iCheck('check');
                $(this).parents('.note-list-action').parents('.note-list-item').addClass('note-item-selected');
            } else {
                $(this).iCheck('uncheck');
                $(this).parents('.note-list-action').parents('.note-list-item').removeClass('note-item-selected');
            }
        });

        NoteCheckboxes.on('ifChanged', function (event) {
            if (NoteCheckboxes.filter(':checked').length == NoteCheckboxes.length) {
                NoteCheckAll.prop('checked', 'checked');
            } else {
                NoteCheckAll.removeProp('checked');
            }
            NoteCheckAll.iCheck('update');
        });
    };

    SelectAllNote();

    var wh = $(window).height();
    var AppsContainer = $('.apps-container');
    if (AppsContainer.length) {
        var AppsTopOfset = AppsContainer.offset().top;
    }
    var AppsCalH = wh - AppsTopOfset;


    if ($.summernote) {
        $('.compose-note').summernote({
            height: AppsCalH,
            focus: true,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']]
            ]
        });
    }


    $("[data-event=fullscreen]").on('click', function () {
        var NoteFullScreen = $('.note-body').hasClass('full-screen');
        if (NoteFullScreen) {
            $('.note-body').removeClass('full-screen');
        } else {
            $('.note-body').addClass('full-screen');
        }
    });

    $('.btn-list-serach').on('click', function () {
        $('.list-toolbar').children('.list-search-form').fadeIn('fast');
        $('.list-search-input').focus();
    });

    $('.s-input-close').on('click', function () {
        $('.list-toolbar ').children('.list-search-form').fadeOut('fast');
    });


    function NoteWithSidebar() {
        var NoteSideW = $('.note-sidebar').width();
        var NoteListW = $('.note-list-wrap').width();
        var NoteBodyCal = $(window).width() - (NoteSideW + NoteListW);
        $('.note-body').css({
            'width': (NoteBodyCal - $('.left-aside').width()) + 'px'
        });
    }
    NoteWithSidebar();


    function NoteWithOutSidebar() {
        var NoteListW = $('.note-list-wrap').width();
        var NoteBodyCal = $(window).width() - NoteListW;
        $('.note-body').css({
            'width': (NoteBodyCal - $('.left-aside').width()) + 'px'
        });
    }


    function TaskWithSidebar() {
        var NoteSideW = $('.task-sidebar').width();
        var NoteListW = $('.task-list').width();
        var NoteBodyCal = $(window).width() - (NoteSideW + NoteListW);
        $('.task-body').css({
            'width': (NoteBodyCal - $('.left-aside').width()) + 'px'
        });
    }
    TaskWithSidebar();

    function TaskWithOutSidebar() {
        var NoteListW = $('.task-list').width();
        var NoteBodyCal = $(window).width() - NoteListW;
        $('.task-body').css({
            'width': (NoteBodyCal - $('.left-aside').width()) + 'px'
        });
    }

    $('.note-sidebar-toggle').on('click', function (event) {
        event.preventDefault();

        if (NoteCon.hasClass('hide-note-sidebar')) {
            NoteCon.removeClass('hide-note-sidebar');
            NoteWithSidebar();
            $(this).children('i').removeClass('fa-outdent');
            $(this).children('i').addClass('fa-indent');
        } else {
            NoteCon.addClass('hide-note-sidebar');
            NoteWithOutSidebar();
            $(this).children('i').removeClass('fa-indent');
            $(this).children('i').addClass('fa-outdent');
        }
        if (NoteCon.hasClass('mobile-note-show')) {
            NoteCon.removeClass('mobile-note-show');
        }

    });

    $('.task-sidebar-toggle').on('click', function (event) {
        event.preventDefault();

        if (TaskCon.hasClass('hide-task-sidebar')) {
            TaskCon.removeClass('hide-task-sidebar');
            TaskWithSidebar();
            $(this).children('i').removeClass('fa-outdent');
            $(this).children('i').addClass('fa-indent');
        } else {
            TaskCon.addClass('hide-task-sidebar');
            TaskWithOutSidebar();
            $(this).children('i').removeClass('fa-indent');
            $(this).children('i').addClass('fa-outdent');
        }
        if (TaskCon.hasClass('mobile-task-show')) {
            TaskCon.removeClass('mobile-task-show');
        }
    });



    function ForumCatHeight() {
        var MainHeight = $(window).height();
        var ForumListContainer = $('.forum-list-container');

        if (ForumListContainer.length) {
            var OffsetCal = ForumListContainer.offset().top;

        }

        var CalHeight = MainHeight - OffsetCal;


        $(window).on('scroll', function () {
            var ScrollPos = $(window).scrollTop();

            if (ScrollPos > 60) {
                $('.forum-categories').addClass('fixed-top-side');
            } else {
                $('.forum-categories').removeClass('fixed-top-side');
            }
            var ScrollCal = CalHeight + ScrollPos;
            if (ScrollPos < 60) {
                $('.forum-categories').css({
                    'height': ScrollCal + "px"
                });
            }
        });

        $('.forum-categories').css({
            'height': CalHeight + "px"
        });


    }
    ForumCatHeight();

    $(window).smartresize(function () {
        NoteWithSidebar();
        NoteWithOutSidebar();
        TaskWithSidebar();
        TaskWithOutSidebar();
        ForumCatHeight();

    });



    if ($.fn.select2) {
        $('.tickets-switch').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });
        $('.forum-select').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });
        $('.topic-tags').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });
        $('.status-select').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });
        $('.status-select-all').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });



    }


    if ($.fn.mentionsInput) {
        $('.mention').mentionsInput({
            onDataRequest: function (mode, query, callback) {
                var data = [{
                    id: 1,
                    name: 'Kenneth Auchenberg',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }, {
                    id: 2,
                    name: 'Jon Froda',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }, {
                    id: 3,
                    name: 'Anders Pollas',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }, {
                    id: 4,
                    name: 'Kasper Hulthin',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }, {
                    id: 5,
                    name: 'Andreas Haugstrup',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }, {
                    id: 6,
                    name: 'Pete Lacey',
                    'avatar': 'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif',
                    'type': 'contact'
                }];

                data = _.filter(data, function (item) {
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1
                });

                callback.call(this, data);
            }
        });
    }


    var SyntaxHighlight = $.SyntaxHighlighter;
    if (SyntaxHighlight) {
        SyntaxHighlight.init({
            'theme': 'balupton',
            'themes': ['balupton']
        });
    }


    function ForumTopicAction() {
        var ForumTopic = $('.forum-post-container');
        if (ForumTopic.hasClass('forum-form-hide')) {
            ForumTopic.removeClass('forum-form-hide');
        } else {
            ForumTopic.addClass('forum-form-hide');
        }
    }

    $('.create-topic').on('click', function (e) {
        e.preventDefault();
        ForumTopicAction();

    });

    $('.topic-close').on('click', function (e) {
        e.preventDefault();
        ForumTopicAction();
    });



    function ForumReplyAction() {
        var ForumTopic = $('.forum-reply-post');
        if (ForumTopic.hasClass('forum-reply-hide')) {
            ForumTopic.removeClass('forum-reply-hide');
        } else {
            ForumTopic.addClass('forum-reply-hide');
        }
    }

    $('.reply-forum-topic').on('click', function (e) {
        e.preventDefault();
        ForumReplyAction();

    });

    $('.reply-close').on('click', function (e) {
        e.preventDefault();
        ForumReplyAction();
    });

    $("body").scroll(function (e) {
        e.preventDefault()
    });




    $('.forum-categories').bind('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        } else if (e.type == 'DOMMouseScroll') {
            scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });

    $('.left-navigation, .chat-user-list, .server-stats-content, .aside-notifications-wrap').bind('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type == 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        } else if (e.type == 'DOMMouseScroll') {
            scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });

    $('.forum-aside-toggle').on('click', function (e) {
        var ForumCat = $('.forum-categories');
        if (ForumCat.hasClass('forum-categories-show')) {
            ForumCat.removeClass('forum-categories-show');
            ForumCat.addClass('forum-categories-hide');
        } else {
            ForumCat.removeClass('forum-categories-hide');
            ForumCat.addClass('forum-categories-show');

        }
    });

    var TopicsList = $('.topics-list');
    if (TopicsList.hasClass('condense-topics')) {
        $('.list-condense').addClass('active-btn');
    } else {
        $('.list-expand').addClass('active-btn');
    }

    $('.list-condense').on('click', function () {

        TopicsList.addClass('condense-topics');
        $(this).addClass('active-btn');
        $(this).siblings('.btn').removeClass('active-btn');
    });

    $('.list-expand').on('click', function () {

        if (TopicsList.hasClass('condense-topics')) {
            TopicsList.removeClass('condense-topics');
            $(this).addClass('active-btn');
            $(this).siblings('.btn').removeClass('active-btn');
        }
    });


    /*==Plugins Init==*/

    /*========================
         * ADVANCED FORM ELEMENTS *
     ==========================*/
    /**
     * Tags Input
     * jquery.tagsinput.js
     * tagsinput.css
     * */
    if ($.fn.tagsInput) {
        $('.tags-input').each(function () {
            var tagsType = $(this).data('type')
            var highlightColor = $(this).data('highlight-color')
            if (tagsType === 'tags') {
                $(this).tagsInput({
                    width: 'auto'
                });
            }
            if (tagsType === 'highlighted-tags') {
                $(this).tagsInput({
                    width: 'auto',
                    onChange: function (elem, elem_tags) {
                        var languages = ['php', 'ruby', 'javascript'];
                        $('.tag', elem_tags).each(function () {
                            if ($(this).text().search(new RegExp('\\b(' + languages.join('|') + ')\\b')) >= 0) $(this).css('background-color', highlightColor);
                        });
                    }
                });
            }
        });
    }

    /**
     * Input Mask
     * jquery.mask.js
     *  */
    if ($.fn.mask) {
        $('.date-mask').mask('11/11/1111', {
            placeholder: "__/__/____"
        });
        $('.time-mask').mask('00:00:00', {
            placeholder: "00:00:00"
        });
        $('.date_time-mask').mask('00/00/0000 00:00:00', {
            placeholder: "00/00/0000 00:00:00"
        });
        $('.cep-mask').mask('00000-000', {
            placeholder: "00000-000"
        });
        $('.phone-mask').mask('0000-0000', {
            placeholder: "0000-0000"
        });
        $('.phone_with_ddd-mask').mask('(00) 0000-0000', {
            placeholder: "(00) 0000-0000"
        });
        $('.phone_us-mask').mask('(000) 000-0000', {
            placeholder: "(000) 000-0000"
        });
        $('.mixed-mask').mask('AAA 000-S0S', {
            placeholder: "AAA 000-S0S"
        });
        $('.cpf-mask').mask('000.000.000-00', {
            reverse: true,
            placeholder: "000.000.000-00"
        });
        $('.money-mask').mask('000.000.000.000.000,00', {
            reverse: true,
            placeholder: "000.000.000.000.000,00"
        });
        $('.money2-mask').mask("#.##0,00", {
            reverse: true,
            maxlength: false,
            placeholder: "#.##0,00"
        });
        $('.ip_address-mask').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            placeholder: "0ZZ.0ZZ.0ZZ.0ZZ",
            translation: {
                'Z': {
                    pattern: /[0-9]/,
                    optional: true
                }
            }
        });
        $('.ip_address-mask').mask('099.099.099.099', {
            placeholder: "099.099.099.099"
        });
        $('.percent-mask').mask('##0,00%', {
            reverse: true,
            placeholder: "###,##%"
        });
        $('.clear-if-not-match-mask').mask("00/00/0000", {
            clearIfNotMatch: true,
            placeholder: "__/__/____"
        });
        $('.placeholder-mask').mask("00/00/0000", {
            placeholder: "__/__/____"
        });
    }


    /** 
     * select2.js
     * select2-bootstrap.css
     *  */
    if ($.fn.select2) {
        var placeholder = "Select a State";
        $('.select2, .select2-multiple').select2({
            placeholder: placeholder
        });

        $('.select2-allow-clear').select2({
            allowClear: true,
            placeholder: placeholder
        });
        $('button[data-select2-open]').click(function () {
            $('#' + $(this).data('select2-open')).select2('open');
        });
        var select2OpenEventName = "select2-open";
        $(':checkbox').on("click", function () {
            $(this).parent().nextAll('select').select2("enable", this.checked);
        });
    }

    /**
     * Spinner
     * jquery.bootstrap-touchspin.js
     * bootstrap-touchspin.css
     *  */
    if ($.fn.TouchSpin) {
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='demo1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='demo2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='demo3']").TouchSpin();
        $("input[name='demo3_21']").TouchSpin({
            initval: 40
        });
        $("input[name='demo3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='demo5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });
        $("input[name='demo0']").TouchSpin({});
    }

    /**
     * Bootstrap Filestyle
     * bootstrap-filestyle.js
     **/
    if ($.fn.filestyle) {
        $(":file").filestyle();
    }


    /**
     * selectize.css
     * selectize.js
     * */
    if ($.fn.selectize) {

        var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
            '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

        $('#select-contact').selectize({
            plugins: ['remove_button'],
            persist: false,
            maxItems: null,
            valueField: 'email',
            labelField: 'name',
            searchField: ['name', 'email'],
            options: [{
                email: 'brian@thirdroute.com',
                name: 'Brian Reavis'
            }, {
                email: 'nikola@tesla.com',
                name: 'Nikola Tesla'
            }, {
                email: 'someone@gmail.com'
            }],
            render: {
                item: function (item, escape) {
                    return '<div>' +
                        (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                        (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                        '</div>';
                },
                option: function (item, escape) {
                    var label = item.name || item.email;
                    var caption = item.name ? item.email : null;
                    return '<div>' +
                        '<span class="label">' + escape(label) + '</span>' +
                        (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                        '</div>';
                }
            },
            createFilter: function (input) {
                var match, regex;

                // email@address.com
                regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[0]);

                // name <email@address.com>
                regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[2]);

                return false;
            },
            create: function (input) {
                if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                    return {
                        email: input
                    };
                }
                var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
                if (match) {
                    return {
                        email: match[2],
                        name: $.trim(match[1])
                    };
                }
                alert('Invalid email address.');
                return false;
            }
        });


        $('#input-tags').selectize({
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

        $('#selectize-select').selectize();

    }

    /**
     * Bootstrap Datepicker
     * bootstrap-datepicker.js
     * datepicker.css
     **/

    if ($.fn.datepicker) {
        $('.input-date-picker').datepicker({
            orientation: "bottom",
            daysOfWeekDisabled: "6",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true
        });
        $('.cal-date-picker').datepicker({
            orientation: "bottom",
            daysOfWeekDisabled: "6",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true
        });

        $('.addon-datepicker').datepicker({
            orientation: "bottom",
            daysOfWeekDisabled: "1",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true
        });

        $('.inline-date-picker').datepicker({
            daysOfWeekDisabled: "1",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true
        });

        $('.input-daterange').datepicker({
            orientation: "top",
            daysOfWeekDisabled: "1",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true
        });

    }

    /**
     * Bootstrap daterangepicker
     * daterangepicker.js
     * daterangepicker.css
     **/

    if ($.fn.daterangepicker) {
        $('.input-daterange-datepicker').daterangepicker();
        $('.input-daterange-timepicker').daterangepicker({
            timePicker: true,
            format: 'MM/DD/YYYY h:mm A',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false
        });
        $('.input-limit-datepicker').daterangepicker({
            format: 'MM/DD/YYYY',
            minDate: '06/01/2015',
            maxDate: '06/30/2015',
            dateLimit: {
                days: 6
            }
        });

        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });

    }

    /**
     * Bootstrap colorpicker
     * colorpicker.css
     * colorpicker.js
     *  */

    if ($.fn.colorpicker) {
        $('.demo1').colorpicker();
        $('.demo2').colorpicker();

    }

    /**
     * Bootstrap colorpicker
     * colorpicker.css
     * colorpicker.js
     *  */

    if ($.fn.colorPicker) {

        $('.color').colorPicker();
        $('.elem-color').colorPicker();

    }

    /**
     * Form Validate
     * jquery.validate.js
     *  */
    if ($.fn.validate) {

        $("#commentForm").validate();

        $("#login").validate()

        // validate the form when it is submitted
        var validator = $("#form1").validate({
            errorPlacement: function (error, element) {
                // Append error within linked label
                $(element)
                    .closest("form")
                    .find("label[for='" + element.attr("id") + "']")
                    .append(error);
            },
            errorElement: "span",
            messages: {
                user: {
                    required: " (required)",
                    minlength: " (must be at least 3 characters)"
                },
                password: {
                    required: " (required)",
                    minlength: " (must be between 5 and 12 characters)",
                    maxlength: " (must be between 5 and 12 characters)"
                }
            }
        });

        $(".cancel").click(function () {
            validator.resetForm();
        });

        $("#form2").validate();
    }


    /**
     * summernote-wysiwyg
     * summernote.min.js
     * text-editor.css
     **/

    if ($.summernote) {
        $('.minimal-editor').summernote({
            height: 200,
            focus: false,
            toolbar: [
                ['font', ['bold', 'italic', 'underline']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('.simple-editor').summernote({
            height: 200,
            focus: false,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link', 'picture', 'hr']]
            ]
        });
        $('.full-editor').summernote({
            height: 250,
            focus: false,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']]
            ]
        });


        $("[data-event=fullscreen]").on('click', function () {
            var FullScreen = $('body').hasClass('full-screen');
            if (FullScreen) {
                $('body').removeClass('full-screen');
            } else {
                $('body').addClass('full-screen');
            }
        });


        $("#text-edit").on('click', function () {
            $('.editable-note').summernote({
                focus: true,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'hr']],
                    ['view', ['codeview']]
                ]
            });
        });

        $("#text-save").on('click', function () {
            var aHTML = $('.editable-note').code();
            $('.editable-note').destroy();
        });


        $('.air-note').summernote({
            airMode: true,
            airPopover: [
                ['color', ['color']],
                ['font', ['bold', 'underline', 'clear']],
                ['para', ['ul', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']]
            ]
        });


    }

    /*--jQuery Noty
    * switchery.css
    * switchery.js
    --*/

    try {
        var sw_large = Array.prototype.slice.call(document.querySelectorAll('.switch-large'));

        sw_large.forEach(function (html) {
            var sw_largeGen = new Switchery(html, {
                size: 'large',
                color: '#66bb6a',
                jackColor: '#fff',
                secondaryColor: '#eee',
                jackSecondaryColor: '#fff'
            });
        });

        var sw_m = Array.prototype.slice.call(document.querySelectorAll('.switch-m'));
        sw_m.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#66bb6a',
                jackColor: '#fff',
                secondaryColor: '#eee',
                jackSecondaryColor: '#fff'
            });
        });

        var sw_small = Array.prototype.slice.call(document.querySelectorAll('.switch-small'));
        sw_small.forEach(function (html) {
            var switchery = new Switchery(html, {
                size: 'small',
                color: '#66bb6a',
                jackColor: '#fff',
                secondaryColor: '#eee',
                jackSecondaryColor: '#fff'
            });
        });

        var sw_c = Array.prototype.slice.call(document.querySelectorAll('.switch-c'));
        sw_c.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#15bdc3',
                jackColor: '#fff',
                secondaryColor: '#eee',
                jackSecondaryColor: '#fff'
            });
        });

        var sw_c = Array.prototype.slice.call(document.querySelectorAll('.switch-mini'));
        sw_c.forEach(function (html) {
            var switchery = new Switchery(html, {
                size: 'small',
                color: '#15bdc3',
                jackColor: '#fff',
                secondaryColor: '#eee',
                jackSecondaryColor: '#fff'
            });
        });

    } catch (e) {

    }



    /*
     * jQuery No UI slider
     * jquery.nouislider.css
     * jquery.nouislider.js
     */
    if ($.fn.noUiSlider) {

        $("#slider-range").noUiSlider({
            start: [50, 200],
            connect: true,
            format: wNumb({
                mark: '',
                decimals: 0,
                prefix: '$'
            }),
            range: {
                'min': 0,
                '20%': 100,
                '40%': 200,
                '60%': 300,
                '80%': 400,
                'max': 500
            }

        });
        $('#slider-range').Link('lower').to($('#slider-snap-value-lower'));

        $('#slider-range').Link('upper').to($('#slider-snap-value-upper'));

        $("#slider-range").noUiSlider_pips({
            mode: 'range',
            density: 3,
            format: wNumb({
                decimals: 0,
                prefix: '$'
            })
        });

        var range_all_sliders = {
            'min': [0],
            '10%': [500, 500],
            '50%': [4000, 1000],
            'max': [10000]
        };



        $("#pips-range").noUiSlider({
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
        $("#pips-range-01").noUiSlider({
            start: [4000],
            range: {
                'min': [2000],
                'max': [10000]
            }
        });
        $("#pips-range-02").noUiSlider({
            range: range_all_sliders,
            start: [0, 500],
            connect: true,
            range: {
                'min': 0,
                'max': 1500
            }
        });
        $("#pips-range-vertical").noUiSlider({
            range: range_all_sliders,
            start: 0,
            orientation: 'vertical'
        });
        $("#pips-range-vertical-01").noUiSlider({
            range: range_all_sliders,
            start: [300, 1200],
            connect: true,
            range: {
                'min': 0,
                '20%': 300,
                '80%': 1200,
                'max': 1500
            },
            orientation: 'vertical'
        });
        $("#pips-range-vertical-02").noUiSlider({
            range: range_all_sliders,
            start: [400, 1000],
            connect: true,
            range: {
                'min': 0,
                'max': 1500
            },
            orientation: 'vertical'
        });


        function filter500(value, type) {
            return value % 1000 ? 2 : 1;
        }
        $(".pips-range").noUiSlider_pips({
            mode: 'range',
            density: 3,
            filter: filter500,
            format: wNumb({
                decimals: 2,
                prefix: '$'
            })
        });

    }

    /**
     * Boot Box
     * bootbox.js
     */
    var DemoCallBack = (function () {
        var elem,
            hideHandler,
            that = {};

        that.init = function (options) {
            elem = $(options.selector);
        };

        that.show = function (text) {
            clearTimeout(hideHandler);
            elem.find("span").html(text);
            elem.delay(200).fadeIn().delay(3000).fadeOut();
        };

        return that;
    }());

    DemoCallBack.init({
        "selector": ".bb-alert"
    });

    var bb_demos = {};

    $(document).on("click", "a[data-bb]", function (e) {
        e.preventDefault();
        var type = $(this).data("bb");

        if (typeof bb_demos[type] === 'function') {
            bb_demos[type]();
        }
    });

    // Alert

    bb_demos.alert = function () {
        bootbox.alert("Hello world!", function () {
            DemoCallBack.show("Hello world callback");
        });
    };

    // Confirm
    bb_demos.confirm = function () {
        bootbox.confirm("Are you sure?", function (result) {
            DemoCallBack.show("Confirm result: " + result);
        });
    };

    // Prompt
    bb_demos.prompt = function () {
        bootbox.prompt({
            title: "What is your real name?",
            value: "Kamrujaman Shohel",
            callback: function (result) {
                if (result === null) {
                    DemoCallBack.show("Prompt dismissed");
                } else {
                    DemoCallBack.show("Hi <b>" + result + "</b>");
                }
            }
        });
    }

    // Dialog
    bb_demos.dialog = function () {
        bootbox.dialog({
            message: "I am a custom dialog",
            title: "Custom title",
            buttons: {
                success: {
                    label: "Success!",
                    className: "btn-success",
                    callback: function () {
                        DemoCallBack.show("great success");
                    }
                },
                danger: {
                    label: "Danger!",
                    className: "btn-danger",
                    callback: function () {
                        DemoCallBack.show("uh oh, look out!");
                    }
                },
                main: {
                    label: "Click ME!",
                    className: "btn-primary",
                    callback: function () {
                        DemoCallBack.show("Primary button");
                    }
                }
            }
        });
    }
    // Custom Html Contents
    bb_demos.custom_html = function () {
        bootbox.dialog({
            title: "That html",
            message: '<img src="images/avatar/jaman-01.jpg" alt="image"/><br/> You can also use <b>html</b>'
        });
    }

    // Custom Html Forms
    bb_demos.html_forms = function () {
        bootbox.dialog({
            title: "This is a form in a modal.",
            message: '<div class="row">  ' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Name</label> ' +
                '<div class="col-md-4"> ' +
                '<input id="name" name="name" type="text" placeholder="Your name" class="form-control input-md"> ' +
                '<span class="help-block">Here goes your name</span> </div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="awesomeness">How awesome is this?</label> ' +
                '<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
                '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked"> ' +
                'Really awesome </label> ' +
                '</div><div class="radio"> <label for="awesomeness-1"> ' +
                '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome"> Super awesome </label> ' +
                '</div> ' +
                '</div> </div>' +
                '</form> </div>  </div>',
            buttons: {
                success: {
                    label: "Save",
                    className: "btn-success",
                    callback: function () {
                        var name = $('#name').val();
                        var answer = $("input[name='awesomeness']:checked").val()
                        DemoCallBack.show("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                    }
                }
            }
        });
    }


    /*Edit Delete Alert in Table*/


    $('.m-user-edit').on('click', function (e) {
        e.preventDefault();
        var Ubox = bootbox.dialog({
            title: "Information for: Amery ",
            message: '<div class="row">  ' +
                '<div class="col-md-12"> ' +
                '<form class="form-horizontal"> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Name</label> ' +
                '<div class="col-md-6"> ' +
                '<input id="name" name="name" type="text" value="Amery" placeholder="Your name" class="form-control input-md"> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Address</label> ' +
                '<div class="col-md-6"> ' +
                '<input id="address" name="address" value="Ap #411-3258 Est. Avenue" type="text" placeholder="Address" class="form-control input-md"> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Image</label> ' +
                '<div class="col-md-6"> ' +
                '<img src="images/avatar/adellecharles.jpg" alt="user">' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Change Photo</label> ' +
                '<div class="col-md-6"> ' +
                '<input type="file" class="filestyle" data-input="false">' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="col-md-4 control-label" for="name">Status</label> ' +
                '<div class="col-md-6"> ' +
                '<select class="form-control ubox-status-select">' +
                '<option>Select</option>' +
                '<option>Approve</option>' +
                '<option>Reject</option>' +
                ' <option>Suspend</option>' +
                '<option>Pending</option>' +
                '</select>' +
                '</div> ' +
                '</div> ' +
                '</div> </div>' +
                '</form> </div>  </div>',
            buttons: {
                update: {
                    label: "Update",
                    className: "btn-success",
                    callback: function () {
                        var name = $('#name').val();
                        var answer = $("input[name='awesomeness']:checked").val()
                        DemoCallBack.show("Information updated for <b>" + name + "</b>");
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: "btn-danger",
                    callback: function () {
                        DemoCallBack.show("You have cancel the form");
                    }
                }
            }
        });


        Ubox.find(".filestyle").filestyle({
            input: false
        });
        Ubox.find('.ubox-status-select').select2({
            placeholder: "Select a State",
            maximumSelectionSize: 6,
            minimumResultsForSearch: -1
        });


    });



    $('.btn-toolbar').on('click', '.m-user-delete', function (e) {
        e.preventDefault();
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Information!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $(_this).parentsUntil('tbody').addClass("noty_animated bounceOutRight");
                setTimeout(function () {
                    $(_this).parentsUntil('tbody').remove();
                }, 1000);

                swal("Deleted!", "Information has been deleted.", "success");
            } else {
                console.log('not remvoed');
            }
        });
    });

    /*
     * Sweet Alert
     * sweetalert.css
     * sweetalert.js
     */

    $('.simple-alert').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            confirmButtonColor: "#DD6B55"
        });
    });

    $('.success-alert').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "success",
            confirmButtonColor: "#4caf50"
        });
    });

    $('.warning-btn-ok').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: "Your imaginary file has been deleted.",
                type: "success",
                confirmButtonColor: "#4caf50"
            });
        });
    });
    $('.warning-btn-cancel').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
    });

    $('.warning-custom-icon').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Sweet!",
            text: "Here's a custom image.",
            imageUrl: "images/avatar/jaman-01.jpg"
        });
    });
    $('.warning-custom-html').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "HTML <small>Title</small>!",
            text: 'A custom <span style="color:#F8BB86">html<span> message.',
            html: true
        });
    });

    $('.warning-auto-close').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Auto close alert!",
            text: "I will close in 3 seconds.",
            timer: 3000,
            showConfirmButton: false
        });
    });

    $('.warning-prompt').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "An input!",
            text: "Write something interesting:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Write something"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false
            }
            swal("Nice!", "You wrote: " + inputValue, "success");
        });
    });




    //Animated Numbers

    $.fn.animateNumbers = function (stop, commas, duration, ease) {
        return this.each(function () {
            var $this = $(this);
            var start = parseInt($this.text().replace(/,/g, ""));
            commas = (commas === undefined) ? true : commas;
            $({
                value: start
            }).animate({
                value: stop
            }, {
                duration: duration == undefined ? 500 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function () {
                    $this.text(Math.floor(this.value));
                    if (commas) {
                        $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                    }
                },
                complete: function () {
                    if (parseInt($this.text()) !== stop) {
                        $this.text(stop);
                        if (commas) {
                            $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                        }
                    }
                }
            });
        });
    };


    try {

        $('.number-rotate').appear();
    } catch (e) {

    }



    $(document.body).on('appear', '.number-rotate', function () {
        $('.number-animate').each(function () {
            $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-animation-duration")));
        });
    });


    if ($.fn.fullCalendar) {
        $('#event-calendar').fullCalendar({
            header: {
                left: 'Next',
                center: 'title',
                right: ''
            },
            defaultDate: '2015-08-12',
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                var calMbox = bootbox.dialog({
                    title: "Event Information",
                    message: '<div class="row">  ' +
                        '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' +
                        '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="name">Event Title</label> ' +
                        '<div class="col-md-6"> ' +
                        '<input id="event_title" name="evtitle" type="text" value="Event Title" placeholder="Event Title" class="form-control input-md"> ' +
                        '</div> ' +
                        '</div> ' +
                        '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="name">Description</label> ' +
                        '<div class="col-md-6"> ' +
                        '<input id="event_description" name="evdesc" value="Event Description" type="text" placeholder="Description" class="form-control input-md"> ' +
                        '</div> ' +
                        '</div> ' +
                        '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="name">Select Color</label> ' +
                        '<div class="col-md-6"> ' +
                        '<div class="input-group event-color-picker"><input id="event_color" type="text" value="#0097a7" class="form-control"/><span class="input-group-addon"><i></i></span></div>' +
                        '</div> ' +
                        '</div> ' +
                        '</form> </div>  </div>',
                    buttons: {
                        update: {
                            label: "Save",
                            className: "btn-success",
                            callback: function () {
                                var evTitle = $('#event_title').val();
                                var evDesc = $('#event_description').val();
                                var evColor = $('#event_color').val();
                                var title = evTitle;
                                var eventData;
                                if (title) {
                                    eventData = {
                                        title: evTitle,
                                        start: start,
                                        end: end,
                                        description: evDesc,
                                        color: evColor
                                    };
                                    $('#event-calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                                }

                            }
                        },
                        cancel: {
                            label: "Cancel",
                            className: "btn-danger",
                            callback: function () {

                            }
                        }
                    }


                });

                calMbox.find('.event-color-picker').colorpicker();


                $('#event-calendar').fullCalendar('unselect');

            },
            eventRender: function (event, element) {
                element.popover({
                    title: event.title,
                    html: true,
                    placement: 'top',
                    content: '<div>' + event.description + '</div>' +
                        '<div>Start: ' + moment(event.start).format('MM/DD/YYYY hh:mm') + '</div>' +
                        '<div>End: ' + moment(event.end).format('MM/DD/YYYY hh:mm') + '</div>'
                });

                element.find('div.fc-title').html(element.find('div.fc-title').text());
                $('body').on('click', function (e) {
                    if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                        element.popover('hide');
                });
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [{
                    id: 1,
                    title: 'Long Event',
                    start: '2015-08-01',
                    end: '2015-08-05',
                    description: 'Four days business conference',
                    color: '#43a047'
                },
                {
                    id: 2,
                    title: 'Meeting',
                    start: '2015-08-07',
                    end: '2015-08-07',
                    description: 'Meeting with Nisha Agarwal',
                    color: '#0097a7'
                },
                {
                    id: 3,
                    title: 'Repeating Event',
                    start: '2015-08-09',
                    end: '2015-08-09',
                    description: 'Meeting with Nisha Agarwal',
                    color: '#f9a825'
                },
                {
                    id: 4,
                    title: 'Repeating Event',
                    start: '2015-08-16',
                    end: '2015-08-18',
                    description: 'Meeting with Nisha Agarwal',
                    color: '#009688'
                },
                {
                    id: 5,
                    title: 'Conference',
                    start: '2015-08-11',
                    end: '2015-08-13',
                    description: 'Attend for a software conference',
                    color: '#455a64'
                },
                {
                    id: 6,
                    title: 'Meeting',
                    start: '2015-08-12T10:30:00',
                    end: '2015-08-12T12:30:00',
                    description: 'Meeting with CEO',
                    color: '#ab47bc'
                },
                {
                    id: 7,
                    title: 'Lunch',
                    start: '2015-08-12',
                    end: '2015-08-12',
                    description: 'Lunch with high official',
                    color: '#ef6c00'
                },
                {
                    id: 8,
                    title: 'Meeting',
                    start: '2015-08-12T14:30:00',
                    end: '2015-08-12T12:30:00',
                    description: 'Meeting with VC',
                    color: '#f9a825'
                },
                {
                    id: 9,
                    title: 'Happy Hour',
                    start: '2015-08-12T17:30:00',
                    end: '2015-08-12T12:30:00',
                    description: 'Happy hour with team members',
                    color: '#455a64'
                },
                {
                    id: 10,
                    title: 'Dinner',
                    start: '2015-08-12T20:00:00',
                    end: '2015-08-12T12:30:00',
                    description: 'Dinner with VC',
                    color: '#455a64'
                },
                {
                    id: 11,
                    title: 'Birthday Party',
                    start: '2015-08-13T07:00:00',
                    end: '2015-08-12T12:30:00',
                    description: 'My daughter birthday party',
                    color: '#ef6c00'
                }
            ]






        });




        $('.CalPrev').on('click', function () {
            $('#event-calendar').fullCalendar('prev');
        });
        $('.CalNext').on('click', function () {
            $('#event-calendar').fullCalendar('next');

        });
        $('.CalToday').click(function () {
            $('#event-calendar').fullCalendar('today');
        });

        $('.CalMonthView').on('click', function () {
            $('#event-calendar').fullCalendar('changeView', 'month');
        });
        $('.CalAgendaWeekView').on('click', function () {
            $('#event-calendar').fullCalendar('changeView', 'agendaWeek');

        });
        $('.CalAgendaDayView').click(function () {
            $('#event-calendar').fullCalendar('changeView', 'agendaDay');

        });

        $('.cal-goDate').on('click', function () {
            var GoDate = $('.cal-date-picker').val();
            if (GoDate === "") {
                swal({
                    title: "Oops! Please slect a date",
                    text: "",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok"
                }, function (isConfirm) {
                    if (isConfirm) {
                        $('.cal-date-picker').focus();
                    }
                });


            } else {
                $('#event-calendar').fullCalendar('gotoDate', GoDate);
            }
        });

    }


    if ($.fn.floatlabel) {
        $('input.floatlabel').floatlabel({
            labelClass: "login-label",
            slideInput: true,
            labelStartTop: '0px',
            labelEndTop: '0px',
            paddingOffset: '20px',
            transitionDuration: 0.2,
            transitionEasing: 'ease-in-out',
            focusColor: '#838780',
            blurColor: '#2996cc'
        });
    }


    $('.matmix-icons-list i,.matmix-icons-list span').on('click', function () {
        var fontAttr = $(this).attr("class");
        $('.font-class').text(fontAttr);

    });

});
(function ($, sr) {

    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this,
                args = arguments;

            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize 
    jQuery.fn[sr] = function (fn) {
        return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
    };

})(jQuery, 'smartresize');


;
(function () {

    /**
     * Require the module at `name`.
     *
     * @param {String} name
     * @return {Object} exports
     * @api public
     */

    function require(name) {
        var module = require.modules[name];
        if (!module) throw new Error('failed to require "' + name + '"');

        if (!('exports' in module) && typeof module.definition === 'function') {
            module.client = module.component = true;
            module.definition.call(this, module.exports = {}, module);
            delete module.definition;
        }

        return module.exports;
    }

    /**
     * Meta info, accessible in the global scope unless you use AMD option.
     */

    require.loader = 'component';

    /**
     * Internal helper object, contains a sorting function for semantiv versioning
     */
    require.helper = {};
    require.helper.semVerSort = function (a, b) {
        var aArray = a.version.split('.');
        var bArray = b.version.split('.');
        for (var i = 0; i < aArray.length; ++i) {
            var aInt = parseInt(aArray[i], 10);
            var bInt = parseInt(bArray[i], 10);
            if (aInt === bInt) {
                var aLex = aArray[i].substr(("" + aInt).length);
                var bLex = bArray[i].substr(("" + bInt).length);
                if (aLex === '' && bLex !== '') return 1;
                if (aLex !== '' && bLex === '') return -1;
                if (aLex !== '' && bLex !== '') return aLex > bLex ? 1 : -1;
                continue;
            } else if (aInt > bInt) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }

    /**
     * Find and require a module which name starts with the provided name.
     * If multiple modules exists, the highest semver is used. 
     * This function can only be used for remote dependencies.

     * @param {String} name - module name: `user~repo`
     * @param {Boolean} returnPath - returns the canonical require path if true, 
     *                               otherwise it returns the epxorted module
     */
    require.latest = function (name, returnPath) {
        function showError(name) {
            throw new Error('failed to find latest module of "' + name + '"');
        }
        // only remotes with semvers, ignore local files conataining a '/'
        var versionRegexp = /(.*)~(.*)@v?(\d+\.\d+\.\d+[^\/]*)$/;
        var remoteRegexp = /(.*)~(.*)/;
        if (!remoteRegexp.test(name)) showError(name);
        var moduleNames = Object.keys(require.modules);
        var semVerCandidates = [];
        var otherCandidates = []; // for instance: name of the git branch
        for (var i = 0; i < moduleNames.length; i++) {
            var moduleName = moduleNames[i];
            if (new RegExp(name + '@').test(moduleName)) {
                var version = moduleName.substr(name.length + 1);
                var semVerMatch = versionRegexp.exec(moduleName);
                if (semVerMatch != null) {
                    semVerCandidates.push({
                        version: version,
                        name: moduleName
                    });
                } else {
                    otherCandidates.push({
                        version: version,
                        name: moduleName
                    });
                }
            }
        }
        if (semVerCandidates.concat(otherCandidates).length === 0) {
            showError(name);
        }
        if (semVerCandidates.length > 0) {
            var module = semVerCandidates.sort(require.helper.semVerSort).pop().name;
            if (returnPath === true) {
                return module;
            }
            return require(module);
        }
        // if the build contains more than one branch of the same module
        // you should not use this funciton
        var module = otherCandidates.sort(function (a, b) {
            return a.name > b.name
        })[0].name;
        if (returnPath === true) {
            return module;
        }
        return require(module);
    }

    /**
     * Registered modules.
     */

    require.modules = {};

    /**
     * Register module at `name` with callback `definition`.
     *
     * @param {String} name
     * @param {Function} definition
     * @api private
     */

    require.register = function (name, definition) {
        require.modules[name] = {
            definition: definition
        };
    };

    /**
     * Define a module's exports immediately with `exports`.
     *
     * @param {String} name
     * @param {Generic} exports
     * @api private
     */

    require.define = function (name, exports) {
        require.modules[name] = {
            exports: exports
        };
    };
    require.register("abpetkov~transitionize@0.0.3", function (exports, module) {

        /**
         * Transitionize 0.0.2
         * https://github.com/abpetkov/transitionize
         *
         * Authored by Alexander Petkov
         * https://github.com/abpetkov
         *
         * Copyright 2013, Alexander Petkov
         * License: The MIT License (MIT)
         * http://opensource.org/licenses/MIT
         *
         */

        /**
         * Expose `Transitionize`.
         */

        module.exports = Transitionize;

        /**
         * Initialize new Transitionize.
         *
         * @param {Object} element
         * @param {Object} props
         * @api public
         */

        function Transitionize(element, props) {
            if (!(this instanceof Transitionize)) return new Transitionize(element, props);

            this.element = element;
            this.props = props || {};
            this.init();
        }

        /**
         * Detect if Safari.
         *
         * @returns {Boolean}
         * @api private
         */

        Transitionize.prototype.isSafari = function () {
            return (/Safari/).test(navigator.userAgent) && (/Apple Computer/).test(navigator.vendor);
        };

        /**
         * Loop though the object and push the keys and values in an array.
         * Apply the CSS3 transition to the element and prefix with -webkit- for Safari.
         *
         * @api private
         */

        Transitionize.prototype.init = function () {
            var transitions = [];

            for (var key in this.props) {
                transitions.push(key + ' ' + this.props[key]);
            }

            this.element.style.transition = transitions.join(', ');
            if (this.isSafari()) this.element.style.webkitTransition = transitions.join(', ');
        };
    });

    require.register("ftlabs~fastclick@v0.6.11", function (exports, module) {
        /**
         * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
         *
         * @version 0.6.11
         * @codingstandard ftlabs-jsv2
         * @copyright The Financial Times Limited [All Rights Reserved]
         * @license MIT License (see LICENSE.txt)
         */

        /*jslint browser:true, node:true*/
        /*global define, Event, Node*/


        /**
         * Instantiate fast-clicking listeners on the specificed layer.
         *
         * @constructor
         * @param {Element} layer The layer to listen on
         */
        function FastClick(layer) {
            'use strict';
            var oldOnClick, self = this;


            /**
             * Whether a click is currently being tracked.
             *
             * @type boolean
             */
            this.trackingClick = false;


            /**
             * Timestamp for when when click tracking started.
             *
             * @type number
             */
            this.trackingClickStart = 0;


            /**
             * The element being tracked for a click.
             *
             * @type EventTarget
             */
            this.targetElement = null;


            /**
             * X-coordinate of touch start event.
             *
             * @type number
             */
            this.touchStartX = 0;


            /**
             * Y-coordinate of touch start event.
             *
             * @type number
             */
            this.touchStartY = 0;


            /**
             * ID of the last touch, retrieved from Touch.identifier.
             *
             * @type number
             */
            this.lastTouchIdentifier = 0;


            /**
             * Touchmove boundary, beyond which a click will be cancelled.
             *
             * @type number
             */
            this.touchBoundary = 10;


            /**
             * The FastClick layer.
             *
             * @type Element
             */
            this.layer = layer;

            if (!layer || !layer.nodeType) {
                throw new TypeError('Layer must be a document node');
            }

            /** @type function() */
            this.onClick = function () {
                return FastClick.prototype.onClick.apply(self, arguments);
            };

            /** @type function() */
            this.onMouse = function () {
                return FastClick.prototype.onMouse.apply(self, arguments);
            };

            /** @type function() */
            this.onTouchStart = function () {
                return FastClick.prototype.onTouchStart.apply(self, arguments);
            };

            /** @type function() */
            this.onTouchMove = function () {
                return FastClick.prototype.onTouchMove.apply(self, arguments);
            };

            /** @type function() */
            this.onTouchEnd = function () {
                return FastClick.prototype.onTouchEnd.apply(self, arguments);
            };

            /** @type function() */
            this.onTouchCancel = function () {
                return FastClick.prototype.onTouchCancel.apply(self, arguments);
            };

            if (FastClick.notNeeded(layer)) {
                return;
            }

            // Set up event handlers as required
            if (this.deviceIsAndroid) {
                layer.addEventListener('mouseover', this.onMouse, true);
                layer.addEventListener('mousedown', this.onMouse, true);
                layer.addEventListener('mouseup', this.onMouse, true);
            }

            layer.addEventListener('click', this.onClick, true);
            layer.addEventListener('touchstart', this.onTouchStart, false);
            layer.addEventListener('touchmove', this.onTouchMove, false);
            layer.addEventListener('touchend', this.onTouchEnd, false);
            layer.addEventListener('touchcancel', this.onTouchCancel, false);

            // Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
            // which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
            // layer when they are cancelled.
            if (!Event.prototype.stopImmediatePropagation) {
                layer.removeEventListener = function (type, callback, capture) {
                    var rmv = Node.prototype.removeEventListener;
                    if (type === 'click') {
                        rmv.call(layer, type, callback.hijacked || callback, capture);
                    } else {
                        rmv.call(layer, type, callback, capture);
                    }
                };

                layer.addEventListener = function (type, callback, capture) {
                    var adv = Node.prototype.addEventListener;
                    if (type === 'click') {
                        adv.call(layer, type, callback.hijacked || (callback.hijacked = function (event) {
                            if (!event.propagationStopped) {
                                callback(event);
                            }
                        }), capture);
                    } else {
                        adv.call(layer, type, callback, capture);
                    }
                };
            }

            // If a handler is already declared in the element's onclick attribute, it will be fired before
            // FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
            // adding it as listener.
            if (typeof layer.onclick === 'function') {

                // Android browser on at least 3.2 requires a new reference to the function in layer.onclick
                // - the old one won't work if passed to addEventListener directly.
                oldOnClick = layer.onclick;
                layer.addEventListener('click', function (event) {
                    oldOnClick(event);
                }, false);
                layer.onclick = null;
            }
        }


        /**
         * Android requires exceptions.
         *
         * @type boolean
         */
        FastClick.prototype.deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;


        /**
         * iOS requires exceptions.
         *
         * @type boolean
         */
        FastClick.prototype.deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent);


        /**
         * iOS 4 requires an exception for select elements.
         *
         * @type boolean
         */
        FastClick.prototype.deviceIsIOS4 = FastClick.prototype.deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


        /**
         * iOS 6.0(+?) requires the target element to be manually derived
         *
         * @type boolean
         */
        FastClick.prototype.deviceIsIOSWithBadTarget = FastClick.prototype.deviceIsIOS && (/OS ([6-9]|\d{2})_\d/).test(navigator.userAgent);


        /**
         * Determine whether a given element requires a native click.
         *
         * @param {EventTarget|Element} target Target DOM element
         * @returns {boolean} Returns true if the element needs a native click
         */
        FastClick.prototype.needsClick = function (target) {
            'use strict';
            switch (target.nodeName.toLowerCase()) {

                // Don't send a synthetic click to disabled inputs (issue #62)
                case 'button':
                case 'select':
                case 'textarea':
                    if (target.disabled) {
                        return true;
                    }

                    break;
                case 'input':

                    // File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
                    if ((this.deviceIsIOS && target.type === 'file') || target.disabled) {
                        return true;
                    }

                    break;
                case 'label':
                case 'video':
                    return true;
            }

            return (/\bneedsclick\b/).test(target.className);
        };


        /**
         * Determine whether a given element requires a call to focus to simulate click into element.
         *
         * @param {EventTarget|Element} target Target DOM element
         * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
         */
        FastClick.prototype.needsFocus = function (target) {
            'use strict';
            switch (target.nodeName.toLowerCase()) {
                case 'textarea':
                    return true;
                case 'select':
                    return !this.deviceIsAndroid;
                case 'input':
                    switch (target.type) {
                        case 'button':
                        case 'checkbox':
                        case 'file':
                        case 'image':
                        case 'radio':
                        case 'submit':
                            return false;
                    }

                    // No point in attempting to focus disabled inputs
                    return !target.disabled && !target.readOnly;
                default:
                    return (/\bneedsfocus\b/).test(target.className);
            }
        };


        /**
         * Send a click event to the specified element.
         *
         * @param {EventTarget|Element} targetElement
         * @param {Event} event
         */
        FastClick.prototype.sendClick = function (targetElement, event) {
            'use strict';
            var clickEvent, touch;

            // On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
            if (document.activeElement && document.activeElement !== targetElement) {
                document.activeElement.blur();
            }

            touch = event.changedTouches[0];

            // Synthesise a click event, with an extra attribute so it can be tracked
            clickEvent = document.createEvent('MouseEvents');
            clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
            clickEvent.forwardedTouchEvent = true;
            targetElement.dispatchEvent(clickEvent);
        };

        FastClick.prototype.determineEventType = function (targetElement) {
            'use strict';

            //Issue #159: Android Chrome Select Box does not open with a synthetic click event
            if (this.deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
                return 'mousedown';
            }

            return 'click';
        };


        /**
         * @param {EventTarget|Element} targetElement
         */
        FastClick.prototype.focus = function (targetElement) {
            'use strict';
            var length;

            // Issue #160: on iOS 7, some input elements (e.g. date datetime) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
            if (this.deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time') {
                length = targetElement.value.length;
                targetElement.setSelectionRange(length, length);
            } else {
                targetElement.focus();
            }
        };


        /**
         * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
         *
         * @param {EventTarget|Element} targetElement
         */
        FastClick.prototype.updateScrollParent = function (targetElement) {
            'use strict';
            var scrollParent, parentElement;

            scrollParent = targetElement.fastClickScrollParent;

            // Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
            // target element was moved to another parent.
            if (!scrollParent || !scrollParent.contains(targetElement)) {
                parentElement = targetElement;
                do {
                    if (parentElement.scrollHeight > parentElement.offsetHeight) {
                        scrollParent = parentElement;
                        targetElement.fastClickScrollParent = parentElement;
                        break;
                    }

                    parentElement = parentElement.parentElement;
                } while (parentElement);
            }

            // Always update the scroll top tracker if possible.
            if (scrollParent) {
                scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
            }
        };


        /**
         * @param {EventTarget} targetElement
         * @returns {Element|EventTarget}
         */
        FastClick.prototype.getTargetElementFromEventTarget = function (eventTarget) {
            'use strict';

            // On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
            if (eventTarget.nodeType === Node.TEXT_NODE) {
                return eventTarget.parentNode;
            }

            return eventTarget;
        };


        /**
         * On touch start, record the position and scroll offset.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.onTouchStart = function (event) {
            'use strict';
            var targetElement, touch, selection;

            // Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
            if (event.targetTouches.length > 1) {
                return true;
            }

            targetElement = this.getTargetElementFromEventTarget(event.target);
            touch = event.targetTouches[0];

            if (this.deviceIsIOS) {

                // Only trusted events will deselect text on iOS (issue #49)
                selection = window.getSelection();
                if (selection.rangeCount && !selection.isCollapsed) {
                    return true;
                }

                if (!this.deviceIsIOS4) {

                    // Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
                    // when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
                    // with the same identifier as the touch event that previously triggered the click that triggered the alert.
                    // Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
                    // immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
                    if (touch.identifier === this.lastTouchIdentifier) {
                        event.preventDefault();
                        return false;
                    }

                    this.lastTouchIdentifier = touch.identifier;

                    // If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
                    // 1) the user does a fling scroll on the scrollable layer
                    // 2) the user stops the fling scroll with another tap
                    // then the event.target of the last 'touchend' event will be the element that was under the user's finger
                    // when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
                    // is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
                    this.updateScrollParent(targetElement);
                }
            }

            this.trackingClick = true;
            this.trackingClickStart = event.timeStamp;
            this.targetElement = targetElement;

            this.touchStartX = touch.pageX;
            this.touchStartY = touch.pageY;

            // Prevent phantom clicks on fast double-tap (issue #36)
            if ((event.timeStamp - this.lastClickTime) < 200) {
                event.preventDefault();
            }

            return true;
        };


        /**
         * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.touchHasMoved = function (event) {
            'use strict';
            var touch = event.changedTouches[0],
                boundary = this.touchBoundary;

            if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
                return true;
            }

            return false;
        };


        /**
         * Update the last position.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.onTouchMove = function (event) {
            'use strict';
            if (!this.trackingClick) {
                return true;
            }

            // If the touch has moved, cancel the click tracking
            if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
                this.trackingClick = false;
                this.targetElement = null;
            }

            return true;
        };


        /**
         * Attempt to find the labelled control for the given label element.
         *
         * @param {EventTarget|HTMLLabelElement} labelElement
         * @returns {Element|null}
         */
        FastClick.prototype.findControl = function (labelElement) {
            'use strict';

            // Fast path for newer browsers supporting the HTML5 control attribute
            if (labelElement.control !== undefined) {
                return labelElement.control;
            }

            // All browsers under test that support touch events also support the HTML5 htmlFor attribute
            if (labelElement.htmlFor) {
                return document.getElementById(labelElement.htmlFor);
            }

            // If no for attribute exists, attempt to retrieve the first labellable descendant element
            // the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
            return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
        };


        /**
         * On touch end, determine whether to send a click event at once.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.onTouchEnd = function (event) {
            'use strict';
            var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

            if (!this.trackingClick) {
                return true;
            }

            // Prevent phantom clicks on fast double-tap (issue #36)
            if ((event.timeStamp - this.lastClickTime) < 200) {
                this.cancelNextClick = true;
                return true;
            }

            // Reset to prevent wrong click cancel on input (issue #156).
            this.cancelNextClick = false;

            this.lastClickTime = event.timeStamp;

            trackingClickStart = this.trackingClickStart;
            this.trackingClick = false;
            this.trackingClickStart = 0;

            // On some iOS devices, the targetElement supplied with the event is invalid if the layer
            // is performing a transition or scroll, and has to be re-detected manually. Note that
            // for this to function correctly, it must be called *after* the event target is checked!
            // See issue #57; also filed as rdar://13048589 .
            if (this.deviceIsIOSWithBadTarget) {
                touch = event.changedTouches[0];

                // In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
                targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
                targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
            }

            targetTagName = targetElement.tagName.toLowerCase();
            if (targetTagName === 'label') {
                forElement = this.findControl(targetElement);
                if (forElement) {
                    this.focus(targetElement);
                    if (this.deviceIsAndroid) {
                        return false;
                    }

                    targetElement = forElement;
                }
            } else if (this.needsFocus(targetElement)) {

                // Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
                // Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
                if ((event.timeStamp - trackingClickStart) > 100 || (this.deviceIsIOS && window.top !== window && targetTagName === 'input')) {
                    this.targetElement = null;
                    return false;
                }

                this.focus(targetElement);

                // Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
                if (!this.deviceIsIOS4 || targetTagName !== 'select') {
                    this.targetElement = null;
                    event.preventDefault();
                }

                return false;
            }

            if (this.deviceIsIOS && !this.deviceIsIOS4) {

                // Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
                // and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
                scrollParent = targetElement.fastClickScrollParent;
                if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
                    return true;
                }
            }

            // Prevent the actual click from going though - unless the target node is marked as requiring
            // real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
            if (!this.needsClick(targetElement)) {
                event.preventDefault();
                this.sendClick(targetElement, event);
            }

            return false;
        };


        /**
         * On touch cancel, stop tracking the click.
         *
         * @returns {void}
         */
        FastClick.prototype.onTouchCancel = function () {
            'use strict';
            this.trackingClick = false;
            this.targetElement = null;
        };


        /**
         * Determine mouse events which should be permitted.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.onMouse = function (event) {
            'use strict';

            // If a target element was never set (because a touch event was never fired) allow the event
            if (!this.targetElement) {
                return true;
            }

            if (event.forwardedTouchEvent) {
                return true;
            }

            // Programmatically generated events targeting a specific element should be permitted
            if (!event.cancelable) {
                return true;
            }

            // Derive and check the target element to see whether the mouse event needs to be permitted;
            // unless explicitly enabled, prevent non-touch click events from triggering actions,
            // to prevent ghost/doubleclicks.
            if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

                // Prevent any user-added listeners declared on FastClick element from being fired.
                if (event.stopImmediatePropagation) {
                    event.stopImmediatePropagation();
                } else {

                    // Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
                    event.propagationStopped = true;
                }

                // Cancel the event
                event.stopPropagation();
                event.preventDefault();

                return false;
            }

            // If the mouse event is permitted, return true for the action to go through.
            return true;
        };


        /**
         * On actual clicks, determine whether this is a touch-generated click, a click action occurring
         * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
         * an actual click which should be permitted.
         *
         * @param {Event} event
         * @returns {boolean}
         */
        FastClick.prototype.onClick = function (event) {
            'use strict';
            var permitted;

            // It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
            if (this.trackingClick) {
                this.targetElement = null;
                this.trackingClick = false;
                return true;
            }

            // Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
            if (event.target.type === 'submit' && event.detail === 0) {
                return true;
            }

            permitted = this.onMouse(event);

            // Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
            if (!permitted) {
                this.targetElement = null;
            }

            // If clicks are permitted, return true for the action to go through.
            return permitted;
        };


        /**
         * Remove all FastClick's event listeners.
         *
         * @returns {void}
         */
        FastClick.prototype.destroy = function () {
            'use strict';
            var layer = this.layer;

            if (this.deviceIsAndroid) {
                layer.removeEventListener('mouseover', this.onMouse, true);
                layer.removeEventListener('mousedown', this.onMouse, true);
                layer.removeEventListener('mouseup', this.onMouse, true);
            }

            layer.removeEventListener('click', this.onClick, true);
            layer.removeEventListener('touchstart', this.onTouchStart, false);
            layer.removeEventListener('touchmove', this.onTouchMove, false);
            layer.removeEventListener('touchend', this.onTouchEnd, false);
            layer.removeEventListener('touchcancel', this.onTouchCancel, false);
        };


        /**
         * Check whether FastClick is needed.
         *
         * @param {Element} layer The layer to listen on
         */
        FastClick.notNeeded = function (layer) {
            'use strict';
            var metaViewport;
            var chromeVersion;

            // Devices that don't support touch don't need FastClick
            if (typeof window.ontouchstart === 'undefined') {
                return true;
            }

            // Chrome version - zero for other browsers
            chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1];

            if (chromeVersion) {

                if (FastClick.prototype.deviceIsAndroid) {
                    metaViewport = document.querySelector('meta[name=viewport]');

                    if (metaViewport) {
                        // Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
                        if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
                            return true;
                        }
                        // Chrome 32 and above with width=device-width or less don't need FastClick
                        if (chromeVersion > 31 && window.innerWidth <= window.screen.width) {
                            return true;
                        }
                    }

                    // Chrome desktop doesn't need FastClick (issue #15)
                } else {
                    return true;
                }
            }

            // IE10 with -ms-touch-action: none, which disables double-tap-to-zoom (issue #97)
            if (layer.style.msTouchAction === 'none') {
                return true;
            }

            return false;
        };


        /**
         * Factory method for creating a FastClick object
         *
         * @param {Element} layer The layer to listen on
         */
        FastClick.attach = function (layer) {
            'use strict';
            return new FastClick(layer);
        };


        if (typeof define !== 'undefined' && define.amd) {

            // AMD. Register as an anonymous module.
            define(function () {
                'use strict';
                return FastClick;
            });
        } else if (typeof module !== 'undefined' && module.exports) {
            module.exports = FastClick.attach;
            module.exports.FastClick = FastClick;
        } else {
            window.FastClick = FastClick;
        }

    });

    require.register("component~indexof@0.0.3", function (exports, module) {
        module.exports = function (arr, obj) {
            if (arr.indexOf) return arr.indexOf(obj);
            for (var i = 0; i < arr.length; ++i) {
                if (arr[i] === obj) return i;
            }
            return -1;
        };
    });

    require.register("component~classes@1.2.1", function (exports, module) {
        /**
         * Module dependencies.
         */

        var index = require('component~indexof@0.0.3');

        /**
         * Whitespace regexp.
         */

        var re = /\s+/;

        /**
         * toString reference.
         */

        var toString = Object.prototype.toString;

        /**
         * Wrap `el` in a `ClassList`.
         *
         * @param {Element} el
         * @return {ClassList}
         * @api public
         */

        module.exports = function (el) {
            return new ClassList(el);
        };

        /**
         * Initialize a new ClassList for `el`.
         *
         * @param {Element} el
         * @api private
         */

        function ClassList(el) {
            if (!el) throw new Error('A DOM element reference is required');
            this.el = el;
            this.list = el.classList;
        }

        /**
         * Add class `name` if not already present.
         *
         * @param {String} name
         * @return {ClassList}
         * @api public
         */

        ClassList.prototype.add = function (name) {
            // classList
            if (this.list) {
                this.list.add(name);
                return this;
            }

            // fallback
            var arr = this.array();
            var i = index(arr, name);
            if (!~i) arr.push(name);
            this.el.className = arr.join(' ');
            return this;
        };

        /**
         * Remove class `name` when present, or
         * pass a regular expression to remove
         * any which match.
         *
         * @param {String|RegExp} name
         * @return {ClassList}
         * @api public
         */

        ClassList.prototype.remove = function (name) {
            if ('[object RegExp]' == toString.call(name)) {
                return this.removeMatching(name);
            }

            // classList
            if (this.list) {
                this.list.remove(name);
                return this;
            }

            // fallback
            var arr = this.array();
            var i = index(arr, name);
            if (~i) arr.splice(i, 1);
            this.el.className = arr.join(' ');
            return this;
        };

        /**
         * Remove all classes matching `re`.
         *
         * @param {RegExp} re
         * @return {ClassList}
         * @api private
         */

        ClassList.prototype.removeMatching = function (re) {
            var arr = this.array();
            for (var i = 0; i < arr.length; i++) {
                if (re.test(arr[i])) {
                    this.remove(arr[i]);
                }
            }
            return this;
        };

        /**
         * Toggle class `name`, can force state via `force`.
         *
         * For browsers that support classList, but do not support `force` yet,
         * the mistake will be detected and corrected.
         *
         * @param {String} name
         * @param {Boolean} force
         * @return {ClassList}
         * @api public
         */

        ClassList.prototype.toggle = function (name, force) {
            // classList
            if (this.list) {
                if ("undefined" !== typeof force) {
                    if (force !== this.list.toggle(name, force)) {
                        this.list.toggle(name); // toggle again to correct
                    }
                } else {
                    this.list.toggle(name);
                }
                return this;
            }

            // fallback
            if ("undefined" !== typeof force) {
                if (!force) {
                    this.remove(name);
                } else {
                    this.add(name);
                }
            } else {
                if (this.has(name)) {
                    this.remove(name);
                } else {
                    this.add(name);
                }
            }

            return this;
        };

        /**
         * Return an array of classes.
         *
         * @return {Array}
         * @api public
         */

        ClassList.prototype.array = function () {
            var str = this.el.className.replace(/^\s+|\s+$/g, '');
            var arr = str.split(re);
            if ('' === arr[0]) arr.shift();
            return arr;
        };

        /**
         * Check if class `name` is present.
         *
         * @param {String} name
         * @return {ClassList}
         * @api public
         */

        ClassList.prototype.has =
            ClassList.prototype.contains = function (name) {
                return this.list ?
                    this.list.contains(name) :
                    !!~index(this.array(), name);
            };

    });

    require.register("component~event@0.1.4", function (exports, module) {
        var bind = window.addEventListener ? 'addEventListener' : 'attachEvent',
            unbind = window.removeEventListener ? 'removeEventListener' : 'detachEvent',
            prefix = bind !== 'addEventListener' ? 'on' : '';

        /**
         * Bind `el` event `type` to `fn`.
         *
         * @param {Element} el
         * @param {String} type
         * @param {Function} fn
         * @param {Boolean} capture
         * @return {Function}
         * @api public
         */

        exports.bind = function (el, type, fn, capture) {
            el[bind](prefix + type, fn, capture || false);
            return fn;
        };

        /**
         * Unbind `el` event `type`'s callback `fn`.
         *
         * @param {Element} el
         * @param {String} type
         * @param {Function} fn
         * @param {Boolean} capture
         * @return {Function}
         * @api public
         */

        exports.unbind = function (el, type, fn, capture) {
            el[unbind](prefix + type, fn, capture || false);
            return fn;
        };
    });

    require.register("component~query@0.0.3", function (exports, module) {
        function one(selector, el) {
            return el.querySelector(selector);
        }

        exports = module.exports = function (selector, el) {
            el = el || document;
            return one(selector, el);
        };

        exports.all = function (selector, el) {
            el = el || document;
            return el.querySelectorAll(selector);
        };

        exports.engine = function (obj) {
            if (!obj.one) throw new Error('.one callback required');
            if (!obj.all) throw new Error('.all callback required');
            one = obj.one;
            exports.all = obj.all;
            return exports;
        };

    });

    require.register("component~matches-selector@0.1.5", function (exports, module) {
        /**
         * Module dependencies.
         */

        var query = require('component~query@0.0.3');

        /**
         * Element prototype.
         */

        var proto = Element.prototype;

        /**
         * Vendor function.
         */

        var vendor = proto.matches ||
            proto.webkitMatchesSelector ||
            proto.mozMatchesSelector ||
            proto.msMatchesSelector ||
            proto.oMatchesSelector;

        /**
         * Expose `match()`.
         */

        module.exports = match;

        /**
         * Match `el` to `selector`.
         *
         * @param {Element} el
         * @param {String} selector
         * @return {Boolean}
         * @api public
         */

        function match(el, selector) {
            if (!el || el.nodeType !== 1) return false;
            if (vendor) return vendor.call(el, selector);
            var nodes = query.all(selector, el.parentNode);
            for (var i = 0; i < nodes.length; ++i) {
                if (nodes[i] == el) return true;
            }
            return false;
        }

    });

    require.register("component~closest@0.1.4", function (exports, module) {
        var matches = require('component~matches-selector@0.1.5')

        module.exports = function (element, selector, checkYoSelf, root) {
            element = checkYoSelf ? {
                parentNode: element
            } : element

            root = root || document

            // Make sure `element !== document` and `element != null`
            // otherwise we get an illegal invocation
            while ((element = element.parentNode) && element !== document) {
                if (matches(element, selector))
                    return element
                // After `matches` on the edge case that
                // the selector matches the root
                // (when the root is not the document)
                if (element === root)
                    return
            }
        }

    });

    require.register("component~delegate@0.2.3", function (exports, module) {
        /**
         * Module dependencies.
         */

        var closest = require('component~closest@0.1.4'),
            event = require('component~event@0.1.4');

        /**
         * Delegate event `type` to `selector`
         * and invoke `fn(e)`. A callback function
         * is returned which may be passed to `.unbind()`.
         *
         * @param {Element} el
         * @param {String} selector
         * @param {String} type
         * @param {Function} fn
         * @param {Boolean} capture
         * @return {Function}
         * @api public
         */

        exports.bind = function (el, selector, type, fn, capture) {
            return event.bind(el, type, function (e) {
                var target = e.target || e.srcElement;
                e.delegateTarget = closest(target, selector, true, el);
                if (e.delegateTarget) fn.call(el, e);
            }, capture);
        };

        /**
         * Unbind event `type`'s callback `fn`.
         *
         * @param {Element} el
         * @param {String} type
         * @param {Function} fn
         * @param {Boolean} capture
         * @api public
         */

        exports.unbind = function (el, type, fn, capture) {
            event.unbind(el, type, fn, capture);
        };

    });

    require.register("component~events@1.0.9", function (exports, module) {

        /**
         * Module dependencies.
         */

        var events = require('component~event@0.1.4');
        var delegate = require('component~delegate@0.2.3');

        /**
         * Expose `Events`.
         */

        module.exports = Events;

        /**
         * Initialize an `Events` with the given
         * `el` object which events will be bound to,
         * and the `obj` which will receive method calls.
         *
         * @param {Object} el
         * @param {Object} obj
         * @api public
         */

        function Events(el, obj) {
            if (!(this instanceof Events)) return new Events(el, obj);
            if (!el) throw new Error('element required');
            if (!obj) throw new Error('object required');
            this.el = el;
            this.obj = obj;
            this._events = {};
        }

        /**
         * Subscription helper.
         */

        Events.prototype.sub = function (event, method, cb) {
            this._events[event] = this._events[event] || {};
            this._events[event][method] = cb;
        };

        /**
         * Bind to `event` with optional `method` name.
         * When `method` is undefined it becomes `event`
         * with the "on" prefix.
         *
         * Examples:
         *
         *  Direct event handling:
         *
         *    events.bind('click') // implies "onclick"
         *    events.bind('click', 'remove')
         *    events.bind('click', 'sort', 'asc')
         *
         *  Delegated event handling:
         *
         *    events.bind('click li > a')
         *    events.bind('click li > a', 'remove')
         *    events.bind('click a.sort-ascending', 'sort', 'asc')
         *    events.bind('click a.sort-descending', 'sort', 'desc')
         *
         * @param {String} event
         * @param {String|function} [method]
         * @return {Function} callback
         * @api public
         */

        Events.prototype.bind = function (event, method) {
            var e = parse(event);
            var el = this.el;
            var obj = this.obj;
            var name = e.name;
            var method = method || 'on' + name;
            var args = [].slice.call(arguments, 2);

            // callback
            function cb() {
                var a = [].slice.call(arguments).concat(args);
                obj[method].apply(obj, a);
            }

            // bind
            if (e.selector) {
                cb = delegate.bind(el, e.selector, name, cb);
            } else {
                events.bind(el, name, cb);
            }

            // subscription for unbinding
            this.sub(name, method, cb);

            return cb;
        };

        /**
         * Unbind a single binding, all bindings for `event`,
         * or all bindings within the manager.
         *
         * Examples:
         *
         *  Unbind direct handlers:
         *
         *     events.unbind('click', 'remove')
         *     events.unbind('click')
         *     events.unbind()
         *
         * Unbind delegate handlers:
         *
         *     events.unbind('click', 'remove')
         *     events.unbind('click')
         *     events.unbind()
         *
         * @param {String|Function} [event]
         * @param {String|Function} [method]
         * @api public
         */

        Events.prototype.unbind = function (event, method) {
            if (0 == arguments.length) return this.unbindAll();
            if (1 == arguments.length) return this.unbindAllOf(event);

            // no bindings for this event
            var bindings = this._events[event];
            if (!bindings) return;

            // no bindings for this method
            var cb = bindings[method];
            if (!cb) return;

            events.unbind(this.el, event, cb);
        };

        /**
         * Unbind all events.
         *
         * @api private
         */

        Events.prototype.unbindAll = function () {
            for (var event in this._events) {
                this.unbindAllOf(event);
            }
        };

        /**
         * Unbind all events for `event`.
         *
         * @param {String} event
         * @api private
         */

        Events.prototype.unbindAllOf = function (event) {
            var bindings = this._events[event];
            if (!bindings) return;

            for (var method in bindings) {
                this.unbind(event, method);
            }
        };

        /**
         * Parse `event`.
         *
         * @param {String} event
         * @return {Object}
         * @api private
         */

        function parse(event) {
            var parts = event.split(/ +/);
            return {
                name: parts.shift(),
                selector: parts.join(' ')
            }
        }

    });

    require.register("switchery", function (exports, module) {
        /**
         * Switchery 0.8.1
         * http://abpetkov.github.io/switchery/
         *
         * Authored by Alexander Petkov
         * https://github.com/abpetkov
         *
         * Copyright 2013-2015, Alexander Petkov
         * License: The MIT License (MIT)
         * http://opensource.org/licenses/MIT
         *
         */

        /**
         * External dependencies.
         */

        var transitionize = require('abpetkov~transitionize@0.0.3'),
            fastclick = require('ftlabs~fastclick@v0.6.11'),
            classes = require('component~classes@1.2.1'),
            events = require('component~events@1.0.9');

        /**
         * Expose `Switchery`.
         */

        module.exports = Switchery;

        /**
         * Set Switchery default values.
         *
         * @api public
         */

        var defaults = {
            color: '#64bd63',
            secondaryColor: '#dfdfdf',
            jackColor: '#fff',
            jackSecondaryColor: null,
            className: 'switchery',
            disabled: false,
            disabledOpacity: 0.5,
            speed: '0.4s',
            size: 'default'
        };

        /**
         * Create Switchery object.
         *
         * @param {Object} element
         * @param {Object} options
         * @api public
         */

        function Switchery(element, options) {
            if (!(this instanceof Switchery)) return new Switchery(element, options);

            this.element = element;
            this.options = options || {};

            for (var i in defaults) {
                if (this.options[i] == null) {
                    this.options[i] = defaults[i];
                }
            }

            if (this.element != null && this.element.type == 'checkbox') this.init();
            if (this.isDisabled() === true) this.disable();
        }

        /**
         * Hide the target element.
         *
         * @api private
         */

        Switchery.prototype.hide = function () {
            this.element.style.display = 'none';
        };

        /**
         * Show custom switch after the target element.
         *
         * @api private
         */

        Switchery.prototype.show = function () {
            var switcher = this.create();
            this.insertAfter(this.element, switcher);
        };

        /**
         * Create custom switch.
         *
         * @returns {Object} this.switcher
         * @api private
         */

        Switchery.prototype.create = function () {
            this.switcher = document.createElement('span');
            this.jack = document.createElement('small');
            this.switcher.appendChild(this.jack);
            this.switcher.className = this.options.className;
            this.events = events(this.switcher, this);

            return this.switcher;
        };

        /**
         * Insert after element after another element.
         *
         * @param {Object} reference
         * @param {Object} target
         * @api private
         */

        Switchery.prototype.insertAfter = function (reference, target) {
            reference.parentNode.insertBefore(target, reference.nextSibling);
        };

        /**
         * Set switch jack proper position.
         *
         * @param {Boolean} clicked - we need this in order to uncheck the input when the switch is clicked
         * @api private
         */

        Switchery.prototype.setPosition = function (clicked) {
            var checked = this.isChecked(),
                switcher = this.switcher,
                jack = this.jack;

            if (clicked && checked) checked = false;
            else if (clicked && !checked) checked = true;

            if (checked === true) {
                this.element.checked = true;

                if (window.getComputedStyle) jack.style.left = parseInt(window.getComputedStyle(switcher).width) - parseInt(window.getComputedStyle(jack).width) + 'px';
                else jack.style.left = parseInt(switcher.currentStyle['width']) - parseInt(jack.currentStyle['width']) + 'px';

                if (this.options.color) this.colorize();
                this.setSpeed();
            } else {
                jack.style.left = 0;
                this.element.checked = false;
                this.switcher.style.boxShadow = 'inset 0 0 0 0 ' + this.options.secondaryColor;
                this.switcher.style.borderColor = this.options.secondaryColor;
                this.switcher.style.backgroundColor = (this.options.secondaryColor !== defaults.secondaryColor) ? this.options.secondaryColor : '#fff';
                this.jack.style.backgroundColor = (this.options.jackSecondaryColor !== this.options.jackColor) ? this.options.jackSecondaryColor : this.options.jackColor;
                this.setSpeed();
            }
        };

        /**
         * Set speed.
         *
         * @api private
         */

        Switchery.prototype.setSpeed = function () {
            var switcherProp = {},
                jackProp = {
                    'background-color': this.options.speed,
                    'left': this.options.speed.replace(/[a-z]/, '') / 2 + 's'
                };

            if (this.isChecked()) {
                switcherProp = {
                    'border': this.options.speed,
                    'box-shadow': this.options.speed,
                    'background-color': this.options.speed.replace(/[a-z]/, '') * 3 + 's'
                };
            } else {
                switcherProp = {
                    'border': this.options.speed,
                    'box-shadow': this.options.speed
                };
            }

            transitionize(this.switcher, switcherProp);
            transitionize(this.jack, jackProp);
        };

        /**
         * Set switch size.
         *
         * @api private
         */

        Switchery.prototype.setSize = function () {
            var small = 'switchery-small',
                normal = 'switchery-default',
                large = 'switchery-large';

            switch (this.options.size) {
                case 'small':
                    classes(this.switcher).add(small)
                    break;
                case 'large':
                    classes(this.switcher).add(large)
                    break;
                default:
                    classes(this.switcher).add(normal)
                    break;
            }
        };

        /**
         * Set switch color.
         *
         * @api private
         */

        Switchery.prototype.colorize = function () {
            var switcherHeight = this.switcher.offsetHeight / 2;

            this.switcher.style.backgroundColor = this.options.color;
            this.switcher.style.borderColor = this.options.color;
            this.switcher.style.boxShadow = 'inset 0 0 0 ' + switcherHeight + 'px ' + this.options.color;
            this.jack.style.backgroundColor = this.options.jackColor;
        };

        /**
         * Handle the onchange event.
         *
         * @param {Boolean} state
         * @api private
         */

        Switchery.prototype.handleOnchange = function (state) {
            if (document.dispatchEvent) {
                var event = document.createEvent('HTMLEvents');
                event.initEvent('change', true, true);
                this.element.dispatchEvent(event);
            } else {
                this.element.fireEvent('onchange');
            }
        };

        /**
         * Handle the native input element state change.
         * A `change` event must be fired in order to detect the change.
         *
         * @api private
         */

        Switchery.prototype.handleChange = function () {
            var self = this,
                el = this.element;

            if (el.addEventListener) {
                el.addEventListener('change', function () {
                    self.setPosition();
                });
            } else {
                el.attachEvent('onchange', function () {
                    self.setPosition();
                });
            }
        };

        /**
         * Handle the switch click event.
         *
         * @api private
         */



        /**
         * Mark an individual switch as already handled.
         *
         * @api private
         */

        Switchery.prototype.markAsSwitched = function () {
            this.element.setAttribute('data-switchery', true);
        };

        /**
         * Check if an individual switch is already handled.
         *
         * @api private
         */

        Switchery.prototype.markedAsSwitched = function () {
            return this.element.getAttribute('data-switchery');
        };

        /**
         * Initialize Switchery.
         *
         * @api private
         */

        Switchery.prototype.init = function () {
            this.hide();
            this.show();
            this.setSize();
            this.setPosition();
            this.markAsSwitched();
            this.handleChange();
            this.handleClick();
        };

        /**
         * See if input is checked.
         *
         * @returns {Boolean}
         * @api public
         */

        Switchery.prototype.isChecked = function () {
            return this.element.checked;
        };

        /**
         * See if switcher should be disabled.
         *
         * @returns {Boolean}
         * @api public
         */

        Switchery.prototype.isDisabled = function () {
            return this.options.disabled || this.element.disabled || this.element.readOnly;
        };

        /**
         * Destroy all event handlers attached to the switch.
         *
         * @api public
         */

        Switchery.prototype.destroy = function () {
            this.events.unbind();
        };

        /**
         * Enable disabled switch element.
         *
         * @api public
         */

        Switchery.prototype.enable = function () {
            if (this.options.disabled) this.options.disabled = false;
            if (this.element.disabled) this.element.disabled = false;
            if (this.element.readOnly) this.element.readOnly = false;
            this.switcher.style.opacity = 1;
            this.events.bind('click', 'bindClick');
        };

        /**
         * Disable switch element.
         *
         * @api public
         */

        Switchery.prototype.disable = function () {
            if (!this.options.disabled) this.options.disabled = true;
            if (!this.element.disabled) this.element.disabled = true;
            if (!this.element.readOnly) this.element.readOnly = true;
            this.switcher.style.opacity = this.options.disabledOpacity;
            this.destroy();
        };

    });

    if (typeof exports == "object") {
        module.exports = require("switchery");
    } else if (typeof define == "function" && define.amd) {
        define("Switchery", [], function () {
            return require("switchery");
        });
    } else {
        (this || window)["Switchery"] = require("switchery");
    }
})()
