<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@login')->name('login.form');
Route::get('/keluar', 'IndexController@logouts')->name('logout.form');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/category', 'CategoryController');
Route::resource('/user', 'UserController');
Route::resource('/product', 'ProductController');
Route::get('/transaksi', 'TransaksiController@index')->name('transaksi.index');
Route::get('/transaksi/create', 'TransaksiController@create')->name('transaksi.create');
Route::post('/post/transaksi', 'TransaksiController@store')->name('transaksi.post');
Route::post('/search/producr', 'TransaksiController@searchProduct')->name('search.create');
