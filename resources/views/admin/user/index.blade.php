    <!DOCTYPE html>
    <html>

    <head>

        @include('layout.head')
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"
            type="text/css" />
    </head>


    <body>
        @include('layout.navbar')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"> <i class="dripicons-box"></i> User</h4>
                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <div id="app">
                                    @include('layout.flash-message')
                                </div>
                                <h4 class="mt-0 header-title">Data User</h4>
                                <p class="text-muted m-b-30 font-14">Input data teknisi untuk kelengkapan data pada
                                    transaksi service<code></code>.
                                </p>
                                </p>
                                <div class="box-header buttonDatatable">
                                    <button type="button" class="btn btn-rounded btn-info" data-toggle="modal"
                                        data-target="#modal-center">Create User</button>
                                </div>
                                <div style="overflow-x:auto;">
                                    <table id="datatable-buttons" class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Date of birth
                                                </th>
                                                <th>
                                                    Gender
                                                </th>
                                                <th>
                                                    Create date
                                                </th>
                                                <th>
                                                    Update date
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($getData as $item)
                                            <tr>
                                                <td>{{ $item->name}}</td>
                                                <td>{{ $item->email}}</td>
                                                <td>{{ $item->DateOfbirth}}</td>
                                                <td>@if ($item->gender == 1)
                                                    Male
                                                @else
                                                    Female
                                                @endif</td>
                                                <td>{{ $item->created_at }}</td>
                                                <td>{{$item->updated_at}}</td>
                                                <td>
                                                    <form action="{{ route('user.destroy',$item->id) }}" method="POST">
                                                        <a class="ladda-button btn btn-primary btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit{{ $item->id }}"><i
                                                                class="fa fa-pencil-square-o"
                                                                aria-hidden="true"></i></a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <a type="button"
                                                            class="ladda-button btn btn-danger btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#basic_modal{{ $item->id }}"
                                                            data-animate-modal="jello"><i class="fa fa-trash-o"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                        <a class="ladda-button btn btn-warning btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#modal-centershow{{ $item->id }}"><i
                                                                class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <div id="basic_modal{{ $item->id }}" class="modal fade animated"
                                                            role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal">&times;</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Are You Sure ?</p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                            class="btn btn-danger">Yes</button>
                                                                        <button type="button" class="btn btn-success"
                                                                            data-dismiss="modal">No</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form></td>
                                                    </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->


            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!--Modal add-->
        <div class="modal center-modal fade" id="modal-center" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create User</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <form class="form-horizontal" role="form" action="{{ route('user.store')}}" method="POST">
                    </div>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>First Nama</label>
                            <input type="text" name="name" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Last Nama</label>
                            <input type="text" name="lastName" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Date Of Birth</label>
                            <input type="date" name="DateOfbirth" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" >
                                <option value="">Selet Gender</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>email</label>
                            <input type="text" name="email" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Text input">
                        </div>
                        <div class="modal-footer modal-footer-uniform">
                            <button type="button" class="btn btn-rounded btn-secondary"
                                data-dismiss="modal">Close</button>
                            <button type="submit" id="add" class="btn btn-rounded btn-primary float-right">Save
                                changes</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
{{-- modal edit --}}
        @foreach ($getData as $item)
        <div class="modal center-modal fade" id="modal-edit{{ $item->id}}" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update user</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <form class="form-horizontal" role="form" action="{{  route('user.update',$item->id) }}"
                            method="POST">
                    </div>
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                          <div class="form-group">
                            <label>First Nama</label>
                            <input type="text" name="name" class="form-control" value="{{$item->name}}" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Last Nama</label>
                            <input type="text" name="lastName" class="form-control" value="{{$item->lastName}}" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Date Of Birth</label>
                            <input type="date" name="DateOfbirth" class="form-control" value="{{$item->DateOfbirth}}" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control" >
                                <option value="">Selet Gender</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>email</label>
                            <input type="text" name="email" class="form-control"  value="{{$item->email}}" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" value="{{$item->password}}" placeholder="Text input">
                        </div>
                        
                        <div class="modal-footer modal-footer-uniform">
                            <button type="button" class="btn btn-rounded btn-secondary"
                                data-dismiss="modal">Close</button>
                            <button type="submit" id="add" class="btn btn-rounded btn-primary float-right">Save
                                changes</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- modal show --}}
        @foreach ($getData as $item)
        <div class="modal center-modal fade" id="modal-centershow{{ $item->id }}" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Show User</h5>
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span></button>
                        <form class="form-horizontal" role="form" action=""
                            method="POST">
                    </div>
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label>First Name</label>:
                            <a>{{ $item->name}}</a>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>:
                            <a>{{ $item->lastName}}</a>
                        </div>
                        <div class="form-group">
                            <label>email</label>:
                            <a>{{ $item->email}}</a>
                        </div>
                        <div class="form-group">
                            <label>Date Of Birth</label>:
                            <a>{{ $item->DateOfbirth}}</a>
                        </div>
                        <div class="form-group">
                            <label>Gender</label>:
                            <a>@if ($item->gender == 1)
                                Male
                            @else
                                Female
                            @endif</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach
        @include('layout.footer')

        @include('layout.script')
        <!-- Required datatable js -->
        <script src="{{asset('assetsnew/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/pages/datatables.init.js')}}"></script>
        @include('layout.script')
    </body>

    </html>
