    <!DOCTYPE html>
    <html>

    <head>

        @include('layout.head')
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"
            type="text/css" />
    </head>


    <body>
        @include('layout.navbar')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"> <i class="dripicons-box"></i> Stock</h4>
                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <div id="app">
                                    @include('layout.flash-message')
                                </div>
                                <h4 class="mt-0 header-title">Data Stock</h4>
                                <p class="text-muted m-b-30 font-14">Input data teknisi untuk kelengkapan data pada
                                    transaksi service<code></code>.
                                </p>
                                </p>
                                <div class="box-header buttonDatatable">
                                    <a type="button" class="btn btn-rounded btn-info" href="{{route('transaksi.create')}}">Create product</a>
                                </div>
                                <div style="overflow-x:auto;">
                                    <table id="datatable-buttons" class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    No transaksi
                                                </th>
                                                <th>
                                                    Name Product
                                                </th>
                                                <th>
                                                    imageProduct
                                                </th>
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th>
                                                    created_at
                                                </th>
                                                <th>
                                                    updated_at
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($data as $item)
                                                <tr>
                                                    <td>{{ $item->idTransaksi}}</td>
                                                    <td>{{ $item->nmProduct}}</td>
                                                    <td><img height="100px" src="{{ asset('/images/'.$item->imageProduct) }}"
                                                    class="img img-responsive"></td>
                                                    <td>{{ $item->nmCategory}}</td>
                                                    <td>@if ($item->status == 1)
                                                        Stock in
                                                    @else
                                                        Stock out
                                                    @endif</td>
                                                    <td>{{ $item->stock}}</td>
                                                    <td>{{ $item->created_at}}</td>
                                                    <td>{{ $item->updated_at}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->


            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        
        @include('layout.footer')

        @include('layout.script')
        <!-- Required datatable js -->
        <script src="{{asset('assetsnew/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/pages/datatables.init.js')}}"></script>
        @include('layout.script')
    </body>

    </html>
