    <!DOCTYPE html>
    <html>

    <head>

        @include('layout.head')
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"
            type="text/css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    </head>


    <body>
        @include('layout.navbar')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"> <i class="dripicons-box"></i> Stock</h4>
                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <div id="app">
                                    @include('layout.flash-message')
                                </div>
                                <h4 class="mt-0 header-title">Data Stock</h4>
                                <p class="text-muted m-b-30 font-14">Input data teknisi untuk kelengkapan data pada
                                    transaksi service<code></code>.
                                </p>
                                </p>
                                <div class="box-header buttonDatatable">
                                </div>
                        <form class="form-horizontal" role="form" action="{{route('transaksi.post')}}" method="POST">
                                    @csrf
                                <div >
                                   <div class="row">
                                       <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">No. Transaksi</label>
                                            <div class="col-sm-10">
                                            <input type="text" name="NoTransaksi" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-2 col-form-label">Date Tarsaksi</label>
                                            <div class="col-sm-10">
                                            <input type="date" name="dateTarsaksi" class="form-control">
                                            </div>
                                        </div>
                                       </div>
                                       <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="" class="col-sm-2 col-form-label">Status Transaksi</label>
                                            <div class="col-sm-10">
                                                <select name="StatusTransaksi" id="" class="form-control">
                                                    <option value="1">Stock in</option>
                                                    <option value="2">Stock Out</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="" class="col-sm-2 col-form-label">User</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="idPembelian" class="form-control" value="{{ $user=Auth::user()->name}}">
                                                <input type="hidden" name="idUser" class="form-control" value="{{ $user=Auth::user()->id}}">
                                            </div>
                                        </div>
                                       </div>
                                   </div>
                                   <div class="input_fields_wrap">
                                        <button class="add_field_button btn btn-success btn-sm mb-5">Add Product</button>

                                       <div class="row">
                                           <div class="col-md-4">
                                                <input type="text" name="nmProduct" class="form-control" id="search" placeholder="Nama Product">
                                                <input type="hidden" name="IdProduct[]" class="form-control" id="ids" placeholder="Nama Product">
                                            </div>
                                           <div class="col-md-4">
                                                <input type="text" name="price" class="form-control" id="price" placeholder="Price">
                                            </div>
                                           <div class="col-md-4">
                                                <input type="text" name="qty[]" class="form-control" placeholder="Qty">
                                            </div>
                                       </div>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <a type="submit" class="btn btn-danger" href="/transaksi" style="float: right;"><i class="fa fa-window-close"></i> Clear</a>
                                    <button type="submit" class="btn btn-primary" style="float: left;"><i class="fa fa-save"></i> Save</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->


            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        
        @include('layout.footer')

        @include('layout.script')
        <!-- Required datatable js -->
        <script src="{{asset('assetsnew/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/pages/datatables.init.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

        @include('layout.script')

        <script>
    
    $( "#search" ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "{{ route('search.create') }}",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,
            _token: '{{ csrf_token() }}',
            },
            success: function( data ) {
              response(data);
            }
          });
        },
        select: function (event, ui) {
          $('#search').val(ui.item.label); // display the selected text
          $('#idproduct').val(ui.item.value); // save selected id to input
          $('#price').val(ui.item.price); // save selected id to input
          $('#ids').val(ui.item.ids); // save selected id to input
          return false;
        }
      });
      $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append(' <div class="row">'+
                                           '<div class="col-md-4">'+
                                                '<input type="text" name="nmProduct" class="form-control" id="search' + x + '" placeholder="Nama Product">'+
                                                '<input type="hidden" name="IdProduct" class="form-control" placeholder="Nama Product">'+
                                            '</div>'+
                                           '<div class="col-md-4">'+
                                                '<input type="text" name="price" class="form-control" placeholder="Price">'+
                                            '</div>'+
                                           '<div class="col-md-4">'+
                                                '<input type="text" name="qty" class="form-control" placeholder="Qty">'+
                                            '</div>'+
                                       '</div>'); // add input boxes.
            }
        });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<?php  for ($i=1; $i <= 10; $i++) {  ?>
    <script>
            $( "#search{{ $i }}" ).autocomplete({
            source: function( request, response ) {
              $.ajax({
                url: "{{ route('search.create') }}",
                type: 'post',
                dataType: "json",
                data: {
                  search: request.term,
                _token: '{{ csrf_token() }}',
                },
                success: function( data ) {
                  response(data);
                }
              });
            },
            select: function (event, ui) {
          $('#search').val(ui.item.label); // display the selected text
          $('#idproduct').val(ui.item.value); // save selected id to input
          $('#price').val(ui.item.price); // save selected id to input
              return false;
            }
          });
    </script>
<?php  } ?>

    </body>

    </html>
