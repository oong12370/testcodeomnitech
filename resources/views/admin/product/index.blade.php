    <!DOCTYPE html>
    <html>

    <head>

        @include('layout.head')
        <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"
            type="text/css" />
    </head>


    <body>
        @include('layout.navbar')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"> <i class="dripicons-box"></i> product</h4>
                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

        </div>
        </div>


        <div class="wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-20">
                            <div class="card-body">
                                <div id="app">
                                    @include('layout.flash-message')
                                </div>
                                <h4 class="mt-0 header-title">Data product</h4>
                                <p class="text-muted m-b-30 font-14">Input data teknisi untuk kelengkapan data pada
                                    transaksi service<code></code>.
                                </p>
                                </p>
                                <div class="box-header buttonDatatable">
                                    <button type="button" class="btn btn-rounded btn-info" data-toggle="modal"
                                        data-target="#modal-center">Create product</button>
                                </div>
                                <div style="overflow-x:auto;">
                                    <table id="datatable-buttons" class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Name product
                                                </th>
                                                <th>
                                                    Description
                                                </th>
                                                <th>
                                                    imageProduct
                                                </th>
                                                <th>
                                                    Category
                                                </th>
                                                <th>
                                                    Price
                                                </th>
                                                <th>
                                                    created_at
                                                </th>
                                                <th>
                                                    updated_at
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($getData as $item)
                                            <tr>
                                                <td>{{ $item->nmProduct}}</td>
                                                <td>{{ $item->description}}</td>
                                                <td><img height="100px" src="{{ asset('/images/'.$item->imageProduct) }}"
                                                class="img img-responsive"></td>
                                                <td>{{ $item->nmCategory}}</td>
                                                <td>{{ $item->price}}</td>
                                                <td>{{ $item->created_at }}</td>
                                                <td>{{$item->updated_at}}</td>
                                                <td>
                                                    <form action="{{ route('product.destroy',$item->id) }}" method="POST">
                                                        <a class="ladda-button btn btn-primary btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit{{ $item->id }}"><i
                                                                class="fa fa-pencil-square-o"
                                                                aria-hidden="true"></i></a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <a type="button"
                                                            class="ladda-button btn btn-danger btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#basic_modal{{ $item->id }}"
                                                            data-animate-modal="jello"><i class="fa fa-trash-o"
                                                                aria-hidden="true"></i>
                                                        </a>
                                                        <a class="ladda-button btn btn-warning btn-xs button_normal"
                                                            data-toggle="modal"
                                                            data-target="#modal-centershow{{ $item->id }}"><i
                                                                class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <div id="basic_modal{{ $item->id }}" class="modal fade animated"
                                                            role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal">&times;</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Are You Sure ?</p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                            class="btn btn-danger">Yes</button>
                                                                        <button type="button" class="btn btn-success"
                                                                            data-dismiss="modal">No</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form></td>
                                                    </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->


            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!--Modal add-->
        <div class="modal center-modal fade" id="modal-center" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create product</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <form class="form-horizontal" role="form" action="{{ route('product.store')}}" method="POST" enctype="multipart/form-data">
                    </div>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama product</label>
                            <input type="text" name="nmProduct" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control" placeholder="Text input"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Image product</label>
                            <input type="file" name="imageProduct" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Image product</label>
                            <select name="idCategori" id="" class="form-control">
                                <option value="">Select Category</option>
                                @foreach ($getCategory as $items)
                                    <option value="{{$items->id}}">{{ $items->nmCategory}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" name="price" class="form-control" placeholder="Text input">
                        </div>
                        <div class="modal-footer modal-footer-uniform">
                            <button type="button" class="btn btn-rounded btn-secondary"
                                data-dismiss="modal">Close</button>
                            <button type="submit" id="add" class="btn btn-rounded btn-primary float-right">Save
                                changes</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
{{-- modal edit --}}
        @foreach ($getData as $item)
        <div class="modal center-modal fade" id="modal-edit{{ $item->id}}" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update product</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <form class="form-horizontal" role="form" action="{{  route('product.update',$item->id) }}"
                            method="POST">
                    </div>
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama product</label>
                            <input type="text" name="nmProduct" value="{{$item->nmProduct}}" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control" value="{{$item->description}}"  placeholder="Text input"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Image product</label>
                            <input type="file" name="imageProduct" value="{{$item->imageProduct}}" class="form-control" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <label>Image product</label>
                            <select name="idCategori" id="" class="form-control">
                                <option value="">Select Category</option>
                                @foreach ($getCategory as $items)
                                    <option value="{{$items->id}}">{{ $items->nmCategory}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" name="price" value="{{$item->price}}" class="form-control" placeholder="Text input">
                        </div>
                        
                        <div class="modal-footer modal-footer-uniform">
                            <button type="button" class="btn btn-rounded btn-secondary"
                                data-dismiss="modal">Close</button>
                            <button type="submit" id="add" class="btn btn-rounded btn-primary float-right">Save
                                changes</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- modal show --}}
        @foreach ($getData as $item)
        <div class="modal center-modal fade" id="modal-centershow{{ $item->id }}" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Show product</h5>
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span></button>
                        <form class="form-horizontal" role="form" action=""
                            method="POST">
                    </div>
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name product</label>:
                            <a>{{ $item->nmProduct}}</a>
                        </div>
                        <div class="form-group">
                            <label>Description</label>:
                            <a>{{ $item->description}}</a>
                        </div>
                        <div class="form-group">
                            <label>Iamge</label>:
                            <a>{{ $item->imageProduct}}</a>
                        </div>
                        <div class="form-group">
                            <label>Category</label>:
                            <a>{{ $item->nmCategory}}</a>
                        </div>
                        <div class="form-group">
                            <label>price</label>:
                            <a>{{ $item->price}}</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach
        @include('layout.footer')

        @include('layout.script')
        <!-- Required datatable js -->
        <script src="{{asset('assetsnew/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{asset('assetsnew/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assetsnew/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assetsnew/pages/datatables.init.js')}}"></script>
        @include('layout.script')
    </body>

    </html>
