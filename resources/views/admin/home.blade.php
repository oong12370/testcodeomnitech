<!DOCTYPE html>
<html>

<head>

@include('layout.head')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
</head>


<body>
@include('layout.navbar')
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <form class="float-right app-search">
                    <input type="text" placeholder="Search..." class="form-control">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
                <h4 class="page-title"> <i class="dripicons-meter"></i> Dashboard</h4>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->
<div class="row">
    <div class="col-12 mb-4">
    </div>
</div>
</div>
</div>
<div class="wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card text-center m-b-30">
                    <div class="mb-2 card-body text-muted">
                        <h3 class="text-info">0</h3>
                        Customer
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="card text-center m-b-30">
                    <div class="mb-2 card-body text-muted">
                        <h3 class="text-purple">0</h3>
                        Service Masuk
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="card text-center m-b-30">
                    <div class="mb-2 card-body text-muted">
                        <h3 class="text-primary">0</h3>
                        Service Keluar
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="card text-center m-b-30">
                    <div class="mb-2 card-body text-muted">
                        <h3 class="text-danger">0</h3>
                        Brand
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->


    </div> <!-- end container -->
</div>
<!-- end wrapper -->

@include('layout.footer') 
<!-- Required datatable js -->
<script src="{{asset('assetsnew/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{asset('assetsnew/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{asset('assetsnew/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('assetsnew/pages/datatables.init.js')}}"></script>

</body>

</html>