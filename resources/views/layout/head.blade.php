        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Test Code</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="{{asset('assetsnew/images/logo-dark-large.png')}}">

        <!-- App css -->
        <link href="{{asset('assetsnew/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assetsnew/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assetsnew/css/style.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assetsnew/plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" />
        <!--pluggin CSS -->
        <link href="{{asset('assetsnew/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assetsnew/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
     