
 <!-- Footer -->
 <footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © <?php echo date("Y") ?> Test Code 
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->


<!-- jQuery  -->
<script src="{{asset('assetsnew/js/jquery.min.js')}}"></script>
<script src="{{asset('assetsnew/js/popper.min.js')}}"></script>
<script src="{{asset('assetsnew/js/bootstrap.min.js')}}"></script>


<!-- Plugins js -->
<script src="{{asset('assetsnew/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assetsnew/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assetsnew/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assetsnew/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assetsnew/plugins/sweet-alert2/sweetalert2.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assetsnew/pages/form-advanced.js')}}"></script>

<script src='https://cdn.rawgit.com/admsev/jquery-play-sound/master/jquery.playSound.js'></script>

<!-- App js -->
<script src="{{asset('assetsnew/js/app.js')}}"></script>

<script src="{{asset('assetsnew/plugins/alertify/js/alertify.js')}}"></script>
<script src="{{asset('assetsnew/pages/alertify-init.js')}}"></script>
<script src="https://js.pusher.com/6.0/pusher.min.js"></script>
