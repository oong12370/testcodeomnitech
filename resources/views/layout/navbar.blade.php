    <!-- Loader -->
    <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!-- Image Logo -->
                        <a href="#" class="logo">
                            test Code
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">
                            
                            <!-- notification-->
                        <?php $user=Auth::user()->is_admin?>
                            @if ($user == 2)
                                <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                    <i class="ti-bell noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge"><div class="notif"></div></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <a href="{{ route('service.in')}}">Notification (<span class="notif"></span>) </a>
                                    </div>

                                    <!-- item-->
                                    <div class="notifikasiviewteknisi"></div>

                                </div>
                            </li>
                            @elseif($user == 1)
                            @else
                                <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                    <i class="ti-bell noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge"><div class="notifs"></div></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <a href="{{ route('home')}}">Notification (<span class="notifs"></span>) </a>
                                    </div>

                                    <!-- item-->
                                    <div class="notifikasiview"></div>

                                </div>
                            </li>
                            @endif
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                    @if(empty($users))
                                    <img src="{{asset('assetsnew/images/users/images.jfif')}}" alt="user" class="rounded-circle">
                                    @else
                                    <img src="{{ asset('/images/'.$users[0]->image) }}" alt="user" class="rounded-circle">
                                    @endif
                                    <span class="ml-1">{{ $user=Auth::user()->name}}. <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <a class="dropdown-item" href=""><i class="dripicons-user text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href=""><i class="dripicons-gear text-muted"></i> Settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout.form')}}"><i class="dripicons-exit text-muted"></i> Logout</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                                                  
                            <li class="has-submenu">
                                <a href="{{Route('home')}}"><i class="dripicons-device-desktop"></i>Dashboard</a>
                            </li>
                            <li class="has-submenu">
                                <a href="{{route('category.index')}}"><i class="dripicons-device-desktop"></i>Category</a>
                            </li>
                                                  
                            <li class="has-submenu">
                                <a href="{{route('user.index')}}"><i class="dripicons-device-desktop"></i>User</a>
                            </li>
                        
                            <li class="has-submenu">
                                <a href="{{route('product.index')}}"><i class="dripicons-device-desktop"></i>product</a>
                            </li>
                                                  
                            <li class="has-submenu">
                                <a href="{{route('transaksi.index')}}"><i class="dripicons-device-desktop"></i>Transaksi</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

