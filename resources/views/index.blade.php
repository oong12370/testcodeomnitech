<!doctype html>
<html>

<head>
<meta charset="utf-8"> 
<title>App Service</title>
<!-- App Icons -->
<link rel="shortcut icon" href="{{asset('assetsnew/images/logo-dark-large.png')}}">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/components.css')}}" type="text/css">
</head>
<body class="login-page">
    <div class="page-container">
        <div class="login-branding">
        </div><br><br><br>
        <div class="login-container">
            <img class="login-img-card" src="{{asset('assetsnew/images/logo-square.jpg')}}" alt="login thumb" />
            <div id="app">
                @include('layout.flash-message')
              </div>
            <form class="form-signin" action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" class="switch-mini" /> Remember Me
                    </label>
                </div>
                <button class="btn btn-primary btn-block btn-signin" type="submit">Sign In</button>
            </form>

        </div>
        <div class="create-account">
        </div>

        <div class="login-footer">
            <a href="#">App 1.0.1 {{ date('Y')}}</a>

        </div>

    </div>
    
    <script src="{{ asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/layout.init.js')}}"></script>
</body>


</html>