-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 01, 2020 at 02:33 PM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tukangko_webkoding`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_15_060636_create_customer_table', 2),
(5, '2020_03_15_062701_create_store_table', 2),
(6, '2020_03_15_064015_create_teknisi_table', 2),
(7, '2020_03_15_064038_create_service_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_brand`
--

CREATE TABLE `m_brand` (
  `id` int(11) NOT NULL,
  `nmBrand` varchar(200) NOT NULL,
  `descriptionBrand` text NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_brand`
--

INSERT INTO `m_brand` (`id`, `nmBrand`, `descriptionBrand`, `created_at`, `updated_at`) VALUES
(1, 'EPILADY', '-', '2020-04-14 01:47:58', '2020-04-14 01:48:06'),
(2, 'GLAMPALM', '-', '2020-04-14 01:48:47', '2020-04-14 01:48:47'),
(3, 'JMW', '-', '2020-04-14 01:49:11', '2020-04-14 01:49:11'),
(4, 'REMINGTON', '-', '2020-04-14 01:49:40', '2020-04-14 01:49:40'),
(5, 'REPIT', '-', '2020-04-14 01:50:09', '2020-04-14 01:50:09'),
(6, 'RUSSELL HOBBS', '-', '2020-04-14 01:50:40', '2020-04-14 01:50:40'),
(7, 'STLE UP', '-', '2020-04-14 01:51:20', '2020-04-14 01:51:20'),
(8, 'STYLERUSH', '-', '2020-04-14 01:51:54', '2020-04-14 01:51:54'),
(9, 'TESCOM', '-', '2020-04-14 01:52:17', '2020-04-14 01:52:17'),
(10, 'SS SHINY', '-', '2020-04-14 01:52:55', '2020-04-14 01:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `m_customer`
--

CREATE TABLE `m_customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_customer`
--

INSERT INTO `m_customer` (`id`, `name`, `telp`, `alamat`, `created_at`, `updated_at`) VALUES
(6, 'Via', '+6282387673246', '-', '2020-04-29 20:07:42', '2020-04-29 20:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `m_service`
--

CREATE TABLE `m_service` (
  `id` int(10) UNSIGNED NOT NULL,
  `tglTerima` date NOT NULL,
  `noSf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pengiriman` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tglGaransi` date DEFAULT NULL,
  `kelengkapan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kerusakan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `realisasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biaya` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tglSelesai` date DEFAULT NULL,
  `tglAmbil` date DEFAULT NULL,
  `tglBayar` date DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tekisi` int(11) DEFAULT NULL,
  `store` int(11) NOT NULL,
  `brand` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `admin` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_service`
--

INSERT INTO `m_service` (`id`, `tglTerima`, `noSf`, `pengiriman`, `type`, `tglGaransi`, `kelengkapan`, `kerusakan`, `realisasi`, `biaya`, `tglSelesai`, `tglAmbil`, `tglBayar`, `description`, `sn`, `tekisi`, `store`, `brand`, `customer`, `status`, `admin`, `created_at`, `updated_at`) VALUES
(1, '2020-05-01', '644', 'jne', 'dd', NULL, 'barang', 'dd', 'aa', '200000', '2020-05-01', '2020-05-01', '2020-05-01', 'aa', '08689', 2, 1, 2, 6, 1, 'oong', '2020-05-01 00:08:59', '2020-05-01 00:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `m_store`
--

CREATE TABLE `m_store` (
  `id` int(10) UNSIGNED NOT NULL,
  `kodeBest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nmStore` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamatStore` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_store`
--

INSERT INTO `m_store` (`id`, `kodeBest`, `nmStore`, `alamatStore`, `created_at`, `updated_at`) VALUES
(1, 'TKPD', 'Tokopedia', '-', '2020-04-14 01:54:44', '2020-04-14 01:54:44'),
(2, 'shp', 'shopee', '-', '2020-04-14 01:54:57', '2020-04-14 01:54:57'),
(3, 'bl', 'bli bli', '-', '2020-04-14 01:55:21', '2020-04-14 01:55:21'),
(4, 'jd', 'JDID', '-', '2020-04-14 01:55:43', '2020-04-14 01:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `m_teknisi`
--

CREATE TABLE `m_teknisi` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_teknisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp_teknisi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_teknisi` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_teknisi`
--

INSERT INTO `m_teknisi` (`id`, `name_teknisi`, `telp_teknisi`, `alamat_teknisi`, `created_at`, `updated_at`) VALUES
(1, 'Rinto', '0979879', 'jakarta', '2020-04-14 01:43:42', '2020-04-14 01:43:42'),
(2, 'Rawat', '96875', 'jakarta', '2020-04-14 01:44:50', '2020-04-14 01:44:50'),
(3, 'Hendri', '07896856', 'jakarta', '2020-04-14 01:45:07', '2020-04-14 01:45:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'oong', 'oong.julian@gmail.com', NULL, '$2y$10$Mn/KvJLYjXkmSrvAs/pCOOQLs0tF4lstdjxICbjCKRkzTLpnUKybu', 'Gnlg05EXD9locxYMkoBNDSEFIuv7H59oq6PD9rz3U72NXGSVo6YxCkrtUwBB', '2020-03-13 09:49:19', '2020-03-13 09:49:19'),
(23, 'ririn', 'ririn.juwita@ravalindo.com', NULL, '$2y$10$S78mtv/geor/WpAwhNpgxe3KKQFh7DA0/mgz.R0UrHMHdygTezSpW', NULL, '2020-04-14 02:23:47', '2020-04-14 02:23:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_brand`
--
ALTER TABLE `m_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_customer`
--
ALTER TABLE `m_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_service`
--
ALTER TABLE `m_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_store`
--
ALTER TABLE `m_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_teknisi`
--
ALTER TABLE `m_teknisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_brand`
--
ALTER TABLE `m_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_customer`
--
ALTER TABLE `m_customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_service`
--
ALTER TABLE `m_service`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_store`
--
ALTER TABLE `m_store`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_teknisi`
--
ALTER TABLE `m_teknisi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
