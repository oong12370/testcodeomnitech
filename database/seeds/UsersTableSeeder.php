<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'oong',
            'lastName'=>'julian',
            'email' => 'oong.julian@gmail.com',
            'password' => bcrypt('123456'),
            'DateOfbirth'=> '01-01-2020',
            'gender'=> '1',
        ]);
    }
}
